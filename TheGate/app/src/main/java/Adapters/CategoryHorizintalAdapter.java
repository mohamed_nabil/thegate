package Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.im.thegate.Preferences;
import com.im.thegate.R;

import java.util.ArrayList;

import Model.Category;

/**
 * Created by Ibrahim on 7/30/2017.
 */

public class CategoryHorizintalAdapter extends BaseAdapter {
    Context mContext;
    ArrayList<Category> Categories = new ArrayList<>();

    public CategoryHorizintalAdapter(Context context) {
        mContext = context;
        this.Categories = Preferences.categories;
    }

    @Override
    public int getCount() {
        //    return coupons.size();
        return Categories.size();
    }

    @Override
    public Object getItem(int i) {
        return Categories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View convertView;
        if (view == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.category_item, null);
        } else convertView = view;

        LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.linear);
        TextView title = (TextView) convertView.findViewById(R.id.cat_text);
        ImageView img = (ImageView) convertView.findViewById(R.id.cat_image);
        // img.setImageResource(R.drawable.copoun_com);

        title.setText(Categories.get(i).getName());
        String icon = Categories.get(i).getIcon();


        Preferences.imageLoader.displayImage(Preferences.ImgPath + icon, img);

        return convertView;
    }

}
