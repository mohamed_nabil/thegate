package Adapters;

import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.im.thegate.MainHome;
import com.im.thegate.Preferences;
import com.im.thegate.R;

import java.util.ArrayList;

import Model.Coupon;
import Model.NobelActivity;
import Model.ShopCardItem;

import static android.view.LayoutInflater.from;

/**
 * Created by Admin on 4/14/2017.
 */

public class CopounsListAdapter extends RecyclerView.Adapter<CopounsListAdapter.ViewHolder> {

    ViewGroup parent;
    NobelActivity activity;
    private Listener listener;
    private ArrayList Copouns;

    public CopounsListAdapter(ArrayList<Coupon> Copouns, NobelActivity activity) {

        this.Copouns = Copouns;
        this.activity = activity;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        CardView cv = (CardView) from(parent.getContext())
                .inflate(R.layout.coupon_card_item, parent, false);
        return new ViewHolder(cv);
    }

    Object getItem(int pos) {
        return Copouns.get(pos);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final CardView cardView = holder.cardView;

        final Coupon coupon = (Coupon) getItem(position);

        ImageView imageView = (ImageView) cardView.findViewById(R.id.image);
        TextView name = (TextView) cardView.findViewById(R.id.name);
        TextView desc = (TextView) cardView.findViewById(R.id.description);
        TextView num = (TextView) cardView.findViewById(R.id.places);
        TextView curr = (TextView) cardView.findViewById(R.id.curr);
        TextView before = (TextView) cardView.findViewById(R.id.before);
        TextView after = (TextView) cardView.findViewById(R.id.after);
        GridLayout gridLayout = (GridLayout) cardView.findViewById(R.id.cities);
        Button Buy = (Button) cardView.findViewById(R.id.Buy);


        Preferences.imageLoader.displayImage(Preferences.ImgPath + coupon.getImage(0), imageView);

        gridLayout.removeAllViews();
        ArrayList<String> arr = coupon.getCitiesNames();
        for (int i = 0; i < arr.size(); i++) {
            Button v = (Button) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.cityitem, parent, false);
            v.setText(arr.get(i));
            gridLayout.addView(v);
            if (i == 1) break;

        }
        Log.e("arrsize", arr.size() + "");
        num.setText("" + arr.size() + "");

        if (coupon.getIs_book().equals("0")) {
            before.setText(coupon.getPrice_before());
            before.setPaintFlags(before.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            after.setText(coupon.getDeal_price());
            curr.setText(Preferences.country.getCurrency());
            Buy.setVisibility(View.VISIBLE);
            Buy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    activity.ShowMessage("Item Added to your Shopping Card");
                    ShopCardItem item = new ShopCardItem();
                    item.Amount = 1;
                    item.coupon = coupon;
                    item.Total_price = Double.valueOf(coupon.getDeal_price());
                    MainHome.ShopCardList.add(item);
                }
            });
        } else {
            curr.setText(coupon.getBook_info());

            Buy.setVisibility(View.GONE);

        }

        name.setText(coupon.getCoupon_name());
        desc.setText(coupon.getCoupon_short_Details());

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return Copouns.size();
    }

    public interface Listener {
        void onClick(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }
}
