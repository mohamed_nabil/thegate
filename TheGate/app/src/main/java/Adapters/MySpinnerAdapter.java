package Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by FCI on 1/14/2017.
 */
public class MySpinnerAdapter extends ArrayAdapter<String> {
    // Initialise custom font, for example:
//    String type;
    // (In reality I used a manager which caches the Typeface objects)
    // Typeface font = FontManager.getInstance().getFont(getContext(), BLAMBOT);

    public MySpinnerAdapter(Context context, int resource, String[] items) {
        super(context, resource, items);

    }

    public MySpinnerAdapter(Context context, int resource, ArrayList<String> items) {
        super(context, resource, items);

    }

    // Affects default (closed) state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setTextColor(Color.parseColor("#ffffff"));
        view.setGravity(Gravity.CENTER);
        view.setTextSize(15);

        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setTextColor(Color.parseColor("#FFFFFF"));
        view.setTextSize(17);
        view.setPadding(10, 0, 10, 0);
        view.setGravity(Gravity.CENTER);
        view.setBackgroundColor(Color.parseColor("#BA8B3D"));
        return view;
    }
}