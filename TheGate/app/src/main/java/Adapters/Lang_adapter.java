package Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.im.thegate.MainHome;
import com.im.thegate.R;

/**
 * Created by Ibrahim on 7/28/2017.
 */

public class Lang_adapter extends BaseAdapter {
    Context mContext;
    Activity activity;
    int images[] = new int[]{ R.drawable.english};
    String names[] = new String[]{ "english"};
    String codes[] = new String[]{ "en"};

    SharedPreferences sharedPreferences;

    public Lang_adapter(Context context,Activity activity) {
        mContext = context;
        this.activity=activity;
        sharedPreferences = mContext.getSharedPreferences("gate_file", Context.MODE_PRIVATE);

    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int i) {
        return names[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        View convertView = LayoutInflater.from(mContext).inflate(R.layout.country_item, null);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.image);
        title.setText(names[i]);
        imageView.setImageResource(images[i]);

        Typeface font = Typeface.createFromAsset(
                mContext.getAssets(),
                "fonts/GESSTwoLight.otf");
        title.setTypeface(font);


        return convertView;
    }
}
