package Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.im.thegate.Preferences;
import com.im.thegate.R;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

import static com.im.thegate.Preferences.imageLoader;

/**
 * Created by Admin on 8/29/2017.
 */

public class ImagesPagerAdapter extends PagerAdapter {
    LayoutInflater mLayoutInflater;
    ArrayList<String> data;

    boolean isAlive;

    public ImagesPagerAdapter(Context context, ArrayList data) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        isAlive = true;
    }


    @Override
    public int getCount() {
        Log.e("count", data.size() + "");
        return data.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.image_item, null, false);

        ImageView offer_image = (ImageView) itemView.findViewById(R.id.image);
        final ProgressBar progressBar = (ProgressBar) itemView.findViewById(R.id.progress);

        imageLoader.displayImage(Preferences.ImgPath + data.get(position), offer_image, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

}