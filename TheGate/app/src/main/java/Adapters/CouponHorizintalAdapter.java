package Adapters;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.im.thegate.Preferences;
import com.im.thegate.R;

import java.util.ArrayList;

import Model.Coupon;

/**
 * Created by Ibrahim on 7/30/2017.
 */

public class CouponHorizintalAdapter extends BaseAdapter {
    Context mContext;
    ArrayList<Coupon> coupons = new ArrayList<>();

    int width, height;
    public CouponHorizintalAdapter(Context context, ArrayList coupons) {
        mContext = context;
        this.coupons = coupons;
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;

    }

    @Override
    public int getCount() {
        //    return coupons.size();
        return coupons.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return coupons.get(i - 1);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View convertView;

        if (view == null)
            convertView = LayoutInflater.from(mContext).inflate(R.layout.discount_coupon_item, null);
        else convertView = view;

        ViewGroup.LayoutParams layoutParams = new LinearLayout.LayoutParams(80 * width / 100, height);
        LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.linear);
        linearLayout.setLayoutParams(layoutParams);

        TextView title = (TextView) convertView.findViewById(R.id.title);
        ImageView mainimage = (ImageView) convertView.findViewById(R.id.coupon_image);
        ImageView cat_icon = (ImageView) convertView.findViewById(R.id.cat_icon);

        if (i == 0) {
            if (Preferences.lang.equals("en")) {
                mainimage.setImageResource(R.drawable.allcoupon);
                title.setText("View All");
            } else {
                mainimage.setImageResource(R.drawable.allcouponar);
                title.setText("مشاهدة الكل");

            }

            cat_icon.setImageResource(R.drawable.allcouponicon);

        } else {
            Coupon c = (Coupon) getItem(i);
            if (Preferences.lang.equals("en"))
                title.setText(c.getCoupon_name());

            Preferences.imageLoader.displayImage(Preferences.ImgPath + c.getImage(0), mainimage);
            Preferences.imageLoader.displayImage(Preferences.ImgPath + c.getCat_img(), cat_icon);

        }

        return convertView;
    }
}
