package Adapters;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.im.thegate.R;

import java.util.ArrayList;

import Model.Store;

/**
 * Created by Ibrahim on 7/30/2017.
 */

public class StoresMapHorizintalAdapter extends BaseAdapter {
    Context mContext;
    ArrayList<Store> stores = new ArrayList<>();
    int height;
    int width;

    public StoresMapHorizintalAdapter(Context context, ArrayList<Store> stores) {
        mContext = context;
        this.stores = stores;
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
    }

    @Override
    public int getCount() {
        //    return coupons.size();
        return stores.size();
    }

    @Override
    public Object getItem(int i) {
        return stores.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View convertView;
        if (view == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.store_map_item, null);
        } else convertView = view;

        ViewGroup.LayoutParams layoutParams = new LinearLayout.LayoutParams(60 * width / 100, LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.linear);
        linearLayout.setLayoutParams(layoutParams);

        TextView title = (TextView) convertView.findViewById(R.id.address);
        title.setText(stores.get(i).getAddress());

        return convertView;
    }

}
