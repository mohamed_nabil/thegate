package Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.im.thegate.R;


/**
 * Created by Ibrahim on 7/28/2017.
 */

public class CustomDialogLang extends Dialog {
    public Activity c;
    public Dialog d;
    int images[] = new int[]{R.drawable.arabic, R.drawable.english};
    String names[] = new String[]{"english"};
    String codes[] = new String[]{"en"};
    SharedPreferences sharedPreferences;
    ListView listView;
    AdapterView.OnItemClickListener listener;

    public CustomDialogLang(Activity a) {
        super(a);
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        sharedPreferences = c.getSharedPreferences("gate_file", Context.MODE_PRIVATE);


        setContentView(R.layout.dialoge_slpash);
        setCancelable(false);
        setCanceledOnTouchOutside(false);

        Typeface font = Typeface.createFromAsset(
                c.getAssets(),
                "fonts/GESSTwoMedium.otf");
        TextView title = (TextView) findViewById(R.id.title);
        title.setTypeface(font);
        Lang_adapter adapter = new Lang_adapter(c.getBaseContext(), c);
        listView = (ListView) findViewById(R.id.listview);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(listener);

    }

    public void AddOnItemClickListenr(AdapterView.OnItemClickListener listener) {
        this.listener = listener;

    }
}
