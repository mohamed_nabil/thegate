package Adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.im.thegate.MainHome;
import com.im.thegate.Preferences;
import com.im.thegate.R;

import java.util.ArrayList;

import Model.NobelActivity;
import Model.ShopCardItem;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Admin on 4/14/2017.
 */

public class ShoppingCardListAdapter extends RecyclerView.Adapter<ShoppingCardListAdapter.ViewHolder> {

    NobelActivity activity;
    private Listener listener;
    private ArrayList<ShopCardItem> items;

    public ShoppingCardListAdapter(NobelActivity activity) {

        this.items = MainHome.ShopCardList;
        this.activity = activity;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shopping_card_item, parent, false);
        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        CardView cardView = holder.cardView;

        final ShopCardItem item = items.get(position);
        ImageView main = (ImageView) cardView.findViewById(R.id.image);
        ImageView delete = (ImageView) cardView.findViewById(R.id.delete);
        TextView title = (TextView) cardView.findViewById(R.id.title);
        ImageView plus = (ImageView) cardView.findViewById(R.id.plus);
        ImageView minus = (ImageView) cardView.findViewById(R.id.minus);
        final TextView amount = (TextView) cardView.findViewById(R.id.amount);
        TextView price = (TextView) cardView.findViewById(R.id.price);
        TextView currency = (TextView) cardView.findViewById(R.id.currency);

        Preferences.imageLoader.displayImage(Preferences.ImgPath + item.coupon.getImage(0), main);

        title.setText(item.coupon.getCoupon_name());
        amount.setText("1");
        price.setText(item.coupon.getDeal_price());
        currency.setText(Preferences.country.getCurrency());
        item.Amount = 1;
        item.Total_price = Double.valueOf(item.coupon.getDeal_price());

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.ShowDialogeConfirmation("warning", "Delete this item from Shopping card!", "confirm", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        items.remove(position);
                        notifyDataSetChanged();
                        listener.onClick(position);
                        sweetAlertDialog.dismissWithAnimation();
                    }
                });
            }
        });
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item.Amount += 1;
                item.Total_price += Double.valueOf(item.coupon.getDeal_price());
                listener.onClick(position);
                amount.setText(item.Amount + "");

            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (item.Amount == 0) return;
                item.Amount -= 1;
                item.Total_price -= Double.valueOf(item.coupon.getDeal_price());
                listener.onClick(position);
                amount.setText(item.Amount + "");

            }
        });

    }

    public double getTotalPrice() {
        double total = 0.0d;
        for (ShopCardItem shopCardItem : items)
            total += shopCardItem.Total_price;
        return total;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface Listener {
        void onClick(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }
}
