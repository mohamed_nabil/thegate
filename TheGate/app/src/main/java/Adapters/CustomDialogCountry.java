package Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.im.thegate.MainHome;
import com.im.thegate.Preferences;
import com.im.thegate.R;
import com.im.thegate.Splash;

import ConnectionHandler.JsonParser;
import ConnectionHandler.ServiceCallback;
import Model.Country;
import Model.NobelActivity;

/**
 * Created by Ibrahim on 8/24/2017.
 */

public class CustomDialogCountry extends Dialog {
    public NobelActivity c;
    public Dialog d;
    SharedPreferences sharedPreferences;

    public CustomDialogCountry(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = (NobelActivity) a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialoge_slpash);

        sharedPreferences=getContext().getSharedPreferences("gate_file", Context.MODE_PRIVATE);

        setCancelable(false);
        setCanceledOnTouchOutside(false);

        Typeface font = Typeface.createFromAsset(
                c.getAssets(),
                "fonts/GESSTwoMedium.otf");

        TextView title = (TextView) findViewById(R.id.title);
        title.setTypeface(font);

        if (Preferences.lang.equals("ar"))
        title.setText("اختر الدوله");
        else    title.setText("Select Country");


        if (Preferences.Countries==null||Preferences.Countries.size()==0){
            LoadCountries();
        }else {
            ShowList();
        }

    }


    public void ShowList(){
         c.runOnUiThread(new Runnable() {
             @Override
             public void run() {
                 Country_adapter adapter = new Country_adapter(c.getBaseContext(),c);
                 ListView listView = (ListView) findViewById(R.id.listview);
                 listView.setAdapter(adapter);

                 listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                     @Override
                     public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                         Intent intent = new Intent(c, MainHome.class);
                         intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                         SharedPreferences.Editor editor=sharedPreferences.edit();
                         editor.putString("country_name",Preferences.Countries.get(i).getName());
                         editor.putString("country_id",Preferences.Countries.get(i).getId());
                         editor.putString("country_icon",Preferences.Countries.get(i).getIcon());
                         editor.putString("country_curr",Preferences.Countries.get(i).getCurrency());
                         editor.apply();
                         Preferences.country=Preferences.Countries.get(i);
                         Preferences.LoadEssentialData();
                         c.startActivity(intent);
                         c.finish();


                     }
                 });

             }
         });


    }
    public void LoadCountries(){

        Preferences.connector.LoadCountries(c.GetLoadingDialogue(),new ServiceCallback() {
        @Override
        public void onSuccess(String Response) {

            Preferences.Countries = JsonParser.fromJsonArray(Response, Country[].class);
            ShowList();


        }
    });

    }
}
