package Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.im.thegate.Preferences;
import com.im.thegate.R;
import com.im.thegate.StoreFragment;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import Model.Store;

/**
 * Created by it on 2/13/2017.
 */

public class StoresListAdapter extends RecyclerView.Adapter<StoresListAdapter.ViewHolder> {

    ArrayList<Store> places;
    Context context;
    ImageLoader imageLoader;
    StoreFragment storeFragment;
    private Listener listener;


    public StoresListAdapter(ArrayList places, Context context, StoreFragment fragment) {
        super();
        this.context = context;
        this.places = places;
        imageLoader = ImageLoader.getInstance();
        this.storeFragment = fragment;

    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        CardView cv;
        if (Preferences.lang.equals("ar"))
            cv = (CardView) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.store_item, parent, false);

        else
            cv = (CardView) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.store_item, parent, false);


        return new ViewHolder(cv);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        CardView cardView = holder.cardView;
        RelativeLayout rel;
        TextView place_name;
        ImageView icon;
        ImageView loc;
        TextView km;

        final Store store = (Store) places.get(position);

        place_name = (TextView) cardView.findViewById(R.id.cat_name);
        icon = (ImageView) cardView.findViewById(R.id.cat_icon);
        loc = (ImageView) cardView.findViewById(R.id.loc);
        km = (TextView) cardView.findViewById(R.id.num);
        rel = (RelativeLayout) cardView.findViewById(R.id.rel);

        Preferences.imageLoader.displayImage(Preferences.ImgPath + store.getMerchant_logo(), icon);
        place_name.setText(store.getMerchant_Name());
        if (Preferences.userlocation != null) {
            if (!store.getLat().equals("") && !store.getLng().equals("")) {
                LatLng place = null;
                try {
                    place = new LatLng(Double.valueOf(store.getLat()), Double.valueOf(store.getLng()));
                    if (Preferences.gps != null) {
                        double dis = Preferences.gps.GetDistance(place, Preferences.userlocation);
                        km.setText(dis + " km.");
                    }

                } catch (NumberFormatException e) {

                }

            }
        }

        loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!store.getLat().equals("") && !store.getLng().equals("")) {
                    LatLng place = null;
                    try {
                        place = new LatLng(Double.valueOf(store.getLat()), Double.valueOf(store.getLng()));
                        if (place != null) {
                            storeFragment.addMarker(place, store.getMerchant_Name(), store.getAddress());
                        } else {
                            Log.e("place", "null");
                        }

                    } catch (NumberFormatException e) {

                    }

                }

            }
        });


        rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(position);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return places.size();
    }

    public interface Listener {
        void onClick(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

}
