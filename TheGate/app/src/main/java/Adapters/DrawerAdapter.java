package Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.im.thegate.Preferences;
import com.im.thegate.R;

import java.util.ArrayList;

/**
 * Created by Admin on 1/6/2017.
 */
public class DrawerAdapter extends BaseAdapter {
    ArrayList<String> headers;
    ArrayList<Integer> Icons;
    LayoutInflater inflater;

    public DrawerAdapter(ArrayList headers, LayoutInflater inflater, ArrayList<Integer> Icons) {
        this.headers = headers;
        this.Icons = Icons;
        this.inflater = inflater;
    }


    public int getCount() {

        if (headers.size() <= 0)
            return 1;
        return headers.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView icon = null;
        TextView header = null;
        if (convertView == null) {
            if (Preferences.lang.equals("ar"))
                convertView = inflater.inflate(R.layout.nav_list_item, null);
            else convertView = inflater.inflate(R.layout.nav_list_item_en, null);


            icon = (ImageView) convertView.findViewById(R.id.nav_list_image);
            header = (TextView) convertView.findViewById(R.id.nav_list_item_header);
            convertView.setTag(headers.get(position));
        } else {

            icon = (ImageView) convertView.findViewById(R.id.nav_list_image);
            header = (TextView) convertView.findViewById(R.id.nav_list_item_header);

        }
        icon.setImageResource(Icons.get(position));

        header.setText(headers.get(position));


        return convertView;
    }

}
