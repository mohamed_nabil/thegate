package Adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.im.thegate.Preferences;
import com.im.thegate.R;

import java.util.ArrayList;

import Model.News;

/**
 * Created by Admin on 4/14/2017.
 */

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.ViewHolder> {

    private Listener listener;
    private ArrayList News;

    public NewsListAdapter(ArrayList<News> News) {

        this.News = News;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_card_item, parent, false);
        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        CardView cardView = holder.cardView;

        News n = (News) News.get(position);
        ImageView main = (ImageView) cardView.findViewById(R.id.newsimage);
        ImageView cat = (ImageView) cardView.findViewById(R.id.newscat);
        TextView title = (TextView) cardView.findViewById(R.id.title);
        TextView date = (TextView) cardView.findViewById(R.id.date);


        Preferences.imageLoader.displayImage(Preferences.ImgPath + n.getImage(0), main);
        Preferences.imageLoader.displayImage(Preferences.ImgPath + n.getCat_icon(), cat);
        title.setText(n.getNews_Title());
        date.setText(n.getDate().split(" ")[0]);


        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return News.size();
    }

    public interface Listener {
        void onClick(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }
}
