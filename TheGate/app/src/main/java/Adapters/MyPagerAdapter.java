package Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by Admin on 1/18/2016.
 */
public class MyPagerAdapter extends FragmentStatePagerAdapter {
    Context mContext;
    ArrayList<Fragment> fragments = new ArrayList<>();
    ArrayList<String> titles;

    public MyPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.mContext = context;
        titles = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    public void addFragment(Fragment fragment) {
        fragments.add(fragment);
    }

    public void addFragment(Fragment fragment, String title) {
        fragments.add(fragment);
        titles.add(title);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_UNCHANGED;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }


}