package Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.im.thegate.Preferences;
import com.im.thegate.R;

import java.util.ArrayList;

import Model.Country;

/**
 * Created by Ibrahim on 7/28/2017.
 */

public class Country_adapter extends BaseAdapter {
    Context mContext;
    Activity activity;
    ArrayList<Country> countryArrayList;
    SharedPreferences sharedPreferences;

    public Country_adapter(Context context, Activity a) {
        mContext = context;
        activity = a;
        sharedPreferences = mContext.getSharedPreferences("gate_file", Context.MODE_PRIVATE);
        countryArrayList = Preferences.Countries;
    }

    @Override
    public int getCount() {

        if (countryArrayList == null) return 0;
        return countryArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return countryArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        View convertView = LayoutInflater.from(mContext).inflate(R.layout.country_item, null);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.image);

        title.setText(countryArrayList.get(i).getName());

        Preferences.imageLoader.displayImage(Preferences.ImgPath + countryArrayList.get(i).getIcon(), imageView);

        Typeface font = Typeface.createFromAsset(
                mContext.getAssets(),
                "fonts/GESSTwoLight.otf");
        title.setTypeface(font);

        return convertView;
    }
}
