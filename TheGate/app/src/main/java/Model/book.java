package Model;

/**
 * Created by Admin on 9/7/2017.
 */

public class book {
    int id;
    String book_deal1;
    String book_deal2;
    String book_image;
    int country_id;
    double deal1_price;
    double deal2_price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBook_deal1() {
        return book_deal1;
    }

    public void setBook_deal1(String book_deal1) {
        this.book_deal1 = book_deal1;
    }

    public String getBook_deal2() {
        return book_deal2;
    }

    public void setBook_deal2(String book_deal2) {
        this.book_deal2 = book_deal2;
    }

    public String getBook_image() {
        return book_image;
    }

    public void setBook_image(String book_image) {
        this.book_image = book_image;
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public double getDeal1_price() {
        return deal1_price;
    }

    public void setDeal1_price(double deal1_price) {
        this.deal1_price = deal1_price;
    }

    public double getDeal2_price() {
        return deal2_price;
    }

    public void setDeal2_price(double deal2_price) {
        this.deal2_price = deal2_price;
    }
}
