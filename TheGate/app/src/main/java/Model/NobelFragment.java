package Model;

import android.content.Context;
import android.support.v4.app.Fragment;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Admin on 9/11/2017.
 */

public class NobelFragment extends Fragment {

    public NobelActivity activity;
    public Context context;
    SweetAlertDialog dialog;
    boolean isvisible;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        activity = (NobelActivity) getActivity();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isvisible = isVisibleToUser;
    }

    public SweetAlertDialog ShowLoadingDialogue() {

        if (activity == null) return null;

        else {
            if (isvisible) {
                dialog = activity.GetLoadingDialogue();

                return dialog;
            }
            return null;
        }

    }

    public void ShowMessage(String Message) {
        if (activity != null)
            activity.ShowMessage(Message);
    }

    public void ShowSignMethod() {
        if (activity != null)
            activity.ShowSignMethode();
    }

    public void ShowDialogeConfirmation(String Title, String message, String confirmText, SweetAlertDialog.OnSweetClickListener confirmAction) {

        activity.ShowDialogeConfirmation(Title, message, confirmText, confirmAction);
    }


}
