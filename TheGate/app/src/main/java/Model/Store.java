package Model;

import java.util.ArrayList;

/**
 * Created by Ibrahim on 11/22/2015.
 */
public class Store {

    String id;
    String merchant_logo;
    String[] imgs;
    String Merchant_Name;
    String Details;
    String phone;
    String website_url;
    String email;
    String whatsapp;
    String instagram;
    String fb_url;
    String twitter_url;
    String video_url;
    String address;
    String city_id;
    String merchent_id;
    String lat;
    String lng;
    ArrayList<feature> features;


    public Store(String id, String merchant_logo, String imgs, String merchant_Name, String details, String phone, String website_url, String email, String whatsapp, String instagram, String fb_url, String twitter_url, String video_url, String address, String city_id, String merchent_id, String lat, String lng, String features) {
        this.id = id;
        this.merchant_logo = merchant_logo;
        Merchant_Name = merchant_Name;
        Details = details;
        this.phone = phone;
        this.website_url = website_url;
        this.email = email;
        this.whatsapp = whatsapp;
        this.instagram = instagram;
        this.fb_url = fb_url;
        this.twitter_url = twitter_url;
        this.video_url = video_url;
        this.address = address;
        this.city_id = city_id;
        this.merchent_id = merchent_id;
        this.lat = lat;
        this.lng = lng;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage(int index) {
        String arr[] = imgs;
        if (arr == null || index >= arr.length)
            return null;
        return arr[index];
    }

    public ArrayList<String> getImages() {
        String arr[] = imgs;
        ArrayList<String> arr1 = new ArrayList<>(4);
        for (String s : arr)
            arr1.add(s);

        return arr1;


    }

    public String getMerchant_logo() {
        return merchant_logo;
    }

    public void setMerchant_logo(String merchant_logo) {
        this.merchant_logo = merchant_logo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String[] getImgs() {
        return imgs;
    }

    public void setImgs(String[] imgs) {
        this.imgs = imgs;
    }

    public String getWebsite_url() {
        return website_url;
    }

    public void setWebsite_url(String website_url) {
        this.website_url = website_url;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getFb_url() {
        return fb_url;
    }

    public void setFb_url(String fb_url) {
        this.fb_url = fb_url;
    }

    public String getTwitter_url() {
        return twitter_url;
    }

    public void setTwitter_url(String twitter_url) {
        this.twitter_url = twitter_url;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getMerchent_id() {
        return merchent_id;
    }

    public void setMerchent_id(String merchent_id) {
        this.merchent_id = merchent_id;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getMerchant_Name() {
        return Merchant_Name;
    }

    public void setMerchant_Name(String merchant_Name) {
        Merchant_Name = merchant_Name;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

    public ArrayList<feature> getFeatures() {
        return features;
    }

    public void setFeatures(ArrayList<feature> features) {
        this.features = features;
    }
}
