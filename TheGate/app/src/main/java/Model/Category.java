package Model;

/**
 * Created by Admin on 8/7/2017.
 */

public class Category {
    private String id;
    private String image;
    private String name;
    private String icon;

    public Category(String id, String image, String name, String icon) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
