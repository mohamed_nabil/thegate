package Model;

/**
 * Created by Admin on 8/7/2017.
 */

public class NewsCategory {
    String icon;
    String image;
    private String id;
    private String name;


    public NewsCategory(String id, String name, String icon, String image) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.image = image;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
