package Model;

/**
 * Created by Admin on 9/23/2017.
 */

public class User {

    String id;
    String name;
    String email;
    String password;
    String phone;
    String age;
    String gender;
    String image;
    String address;
    String country_code;
    String recieve_email;
    String recieve_notification;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getRecieve_email() {
        return recieve_email;
    }

    public void setRecieve_email(String recieve_email) {
        this.recieve_email = recieve_email;
    }

    public String getRecieve_notification() {
        return recieve_notification;
    }

    public void setRecieve_notification(String recieve_notification) {
        this.recieve_notification = recieve_notification;
    }
}
