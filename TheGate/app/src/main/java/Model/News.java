package Model;

import java.util.ArrayList;

/**
 * Created by Admin on 8/7/2017.
 */

public class News {
    int id;
    int newsCategory_id;
    String title;
    String details;
    String is_special;
    String[] imgs;
    String cat_icon;
    String is_available;
    String video_link;
    String Phone;
    String lat;
    String lng;
    String date;

    public News() {

    }

    public News(int id, int newsCategory_id, String news_Title, String news_Details, String is_special, String[] imgs, String cat_icon) {
        this.id = id;
        this.newsCategory_id = newsCategory_id;
        title = news_Title;
        details = news_Details;
        this.is_special = is_special;
        this.imgs = imgs;
        this.cat_icon = cat_icon;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<String> getImages() {
        String arr[] = imgs;
        //.split(",");
        ArrayList<String> arr1 = new ArrayList<>(4);
        for (String s : arr)
            arr1.add(s);

        return arr1;


    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getIs_available() {
        return is_available;
    }

    public void setIs_available(String is_available) {
        this.is_available = is_available;
    }

    public String getVideo_link() {
        return video_link;
    }

    public void setVideo_link(String video_link) {
        this.video_link = video_link;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getIs_special() {
        return is_special;
    }

    public void setIs_special(String is_special) {
        this.is_special = is_special;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNewsCategory_id() {
        return newsCategory_id;
    }

    public void setNewsCategory_id(int newsCategory_id) {
        this.newsCategory_id = newsCategory_id;
    }

    public String getNews_Title() {
        return title;
    }

    public void setNews_Title(String news_Title) {
        title = news_Title;
    }

    public String getNews_Details() {
        return details;
    }

    public void setNews_Details(String news_Details) {
        details = news_Details;
    }

    public String[] getImgs() {
        return imgs;
    }

    public void setImgs(String[] imgs) {
        this.imgs = imgs;
    }

    public String getCat_icon() {
        return cat_icon;
    }

    public void setCat_icon(String cat_icon) {
        this.cat_icon = cat_icon;
    }

    public String getImage(int index) {
        String arr[] = imgs;
        //.split(",");
        if (arr == null || index >= arr.length)
            return null;
        return arr[index];
    }
}
