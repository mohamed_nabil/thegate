package Model;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.im.thegate.Preferences;
import com.im.thegate.SignMethod;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Admin on 9/11/2017.
 */

public class NobelActivity extends AppCompatActivity {
    public NobelActivity activity;
    private SweetAlertDialog pDialog;


    public NobelActivity() {
        activity = this;
    }

    private void PrepareLoadingDialogue() {


        pDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#BA8B3D"));
        //  pDialog.setCustomImage(R.mipmap.ic_launcher);
        if (Preferences.lang.equals("en"))
            pDialog.setTitleText("Loading");
        else pDialog.setTitleText("جاري التحميل");

        pDialog.setCancelable(false);
        pDialog.show();


    }


    public SweetAlertDialog GetLoadingDialogue() {

        if (pDialog == null || pDialog.isShowing() == false)
            PrepareLoadingDialogue();


        return pDialog;

    }

    public SweetAlertDialog ShowMessage(String Message) {
        SweetAlertDialog Dialog = new SweetAlertDialog(this)
                .setTitleText(Message);

      /*  Dialog.get
        Button btn = (Button) Dialog.findViewById(cn.pedant.SweetAlert.R.id.confirm_button);
        btn.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimary));*/
        Dialog.show();
        return Dialog;

    }

    public void ShowSignMethode() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, "please Login first", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(activity, SignMethod.class));

            }
        });

    }

    public void ShowDialogeConfirmation(String Title, String message, String confirmText, SweetAlertDialog.OnSweetClickListener confirmAction) {
        SweetAlertDialog Dialog = new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(Title)
                .setContentText(message)
                .setConfirmText(confirmText)
                .setConfirmClickListener(confirmAction)
                .showCancelButton(true);


      /*  Button btn = (Button) Dialog.findViewById(cn.pedant.SweetAlert.R.id.confirm_button);
        btn.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimary));*/
        Dialog.show();
    }

}
