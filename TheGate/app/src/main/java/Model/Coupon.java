package Model;

import java.util.ArrayList;

/**
 * Created by Admin on 8/7/2017.
 */

public class Coupon {
    String is_book;
    String Merchant_Name;
    String Merchant_Details;
    String coupon_name;
    String coupon_short_Details;
    String coupon_explain;
    String terms;
    String price_before;
    String price_after;
    String deal_price;
    String last_date;
    String deal_view;
    String category_id;
    String cat_img;
    String country_id;
    String merchent_id;
    String is_special;
    String book_info;
    String[] imgs;
    String merchant_logo;
    String phone;
    String website_url;
    String email;
    String whatsapp;
    String instagram;
    String fb_url;
    String twitter_url;
    String video_url;
    String[] cities;
    int rated_sum;
    int rateduser_count;
    private String id;


    public Coupon() {
    }

    public int getRated_sum() {
        return rated_sum;
    }

    public void setRated_sum(int rated_sum) {
        this.rated_sum = rated_sum;
    }

    public int getRateduser_count() {
        return rateduser_count;
    }

    public void setRateduser_count(int rateduser_count) {
        this.rateduser_count = rateduser_count;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite_url() {
        return website_url;
    }

    public void setWebsite_url(String website_url) {
        this.website_url = website_url;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getFb_url() {
        return fb_url;
    }

    public void setFb_url(String fb_url) {
        this.fb_url = fb_url;
    }

    public String getTwitter_url() {
        return twitter_url;
    }

    public void setTwitter_url(String twitter_url) {
        this.twitter_url = twitter_url;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getImage(int index) {
        String arr[] = imgs;
        //.split(",");
        if (arr == null || index >= arr.length)
            return null;
        return arr[index];
    }

    public ArrayList<String> getImages() {
        String arr[] = imgs;
        //.split(",");
        ArrayList<String> arr1 = new ArrayList<>(4);
        for (String s : arr)
            arr1.add(s);

        return arr1;


    }

    public ArrayList<String> getCitiesNames() {
        //.split(",");
        ArrayList<String> arr1 = new ArrayList<>(5);

        if (cities == null) return arr1;
        for (int i = 0; i < cities.length; i++)
            arr1.add(cities[i]);
        return arr1;


    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIs_book() {
        return is_book;
    }

    public void setIs_book(String is_book) {
        this.is_book = is_book;
    }


    public String getPrice_before() {
        return price_before;
    }

    public void setPrice_before(String price_before) {
        this.price_before = price_before;
    }

    public String getPrice_after() {
        return price_after;
    }

    public void setPrice_after(String price_after) {
        this.price_after = price_after;
    }

    public String getDeal_price() {
        return deal_price;
    }

    public void setDeal_price(String deal_price) {
        this.deal_price = deal_price;
    }

    public String getLast_date() {
        return last_date;
    }

    public void setLast_date(String last_date) {
        this.last_date = last_date;
    }

    public String getDeal_view() {
        return deal_view;
    }

    public void setDeal_view(String deal_view) {
        this.deal_view = deal_view;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getMerchent_id() {
        return merchent_id;
    }

    public void setMerchent_id(String merchent_id) {
        this.merchent_id = merchent_id;
    }

    public String getIs_special() {
        return is_special;
    }

    public void setIs_special(String is_special) {
        this.is_special = is_special;
    }

    public String getBook_info() {
        return book_info;
    }

    public void setBook_info(String book_info) {
        this.book_info = book_info;
    }

    public String[] getImgs() {
        return imgs;
    }

    public void setImgs(String[] imgs) {
        this.imgs = imgs;
    }

    public String getMerchant_logo() {
        return merchant_logo;
    }

    public void setMerchant_logo(String merchant_logo) {
        this.merchant_logo = merchant_logo;
    }

    public String getMerchant_Name() {
        return Merchant_Name;
    }

    public void setMerchant_Name(String merchant_Name) {
        Merchant_Name = merchant_Name;
    }

    public String getMerchant_Details() {
        return Merchant_Details;
    }

    public void setMerchant_Details(String merchant_Details) {
        Merchant_Details = merchant_Details;
    }

    public String getCoupon_name() {
        return coupon_name;
    }

    public void setCoupon_name(String coupon_name) {
        this.coupon_name = coupon_name;
    }

    public String getCoupon_short_Details() {
        return coupon_short_Details;
    }

    public void setCoupon_short_Details(String coupon_short_Details) {
        this.coupon_short_Details = coupon_short_Details;
    }

    public String getCoupon_explain() {
        return coupon_explain;
    }

    public void setCoupon_explain(String coupon_explain) {
        this.coupon_explain = coupon_explain;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getCat_img() {
        return cat_img;
    }

    public void setCat_img(String cat_img) {
        this.cat_img = cat_img;
    }

    public String[] getCities() {
        return cities;
    }

    public void setCities(String[] cities) {
        this.cities = cities;
    }
}
