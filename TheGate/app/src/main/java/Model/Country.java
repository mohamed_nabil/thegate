package Model;

/**
 * Created by Admin on 8/17/2017.
 */

public class Country {

    private String id;
    private String name;
    private String icon;
    private String currency;
    private String code;


    public Country(String id, String name, String icon, String currency) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.currency = currency;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
