package MyWidget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Ibrahim on 3/30/2016.
 */
public class EditTextGESS extends EditText {
    public EditTextGESS(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public EditTextGESS(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextGESS(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/GESSTwoLight.otf");
        setTypeface(tf);
    }
}
