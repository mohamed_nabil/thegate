package MyWidget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Ibrahim on 3/30/2016.
 */
public class EditTextcss extends EditText {
    public EditTextcss(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public EditTextcss(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextcss(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/GESSTwoLight.otf");
        setTypeface(tf);
    }
}
