package com.im.thegate;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import Adapters.MyImagePagerAdapter;
import Model.NobelFragment;

/**
 * Created by Admin on 8/7/2017.
 */

public class ProfileMain extends NobelFragment {

    ViewPager viewPager;
    MyImagePagerAdapter pageAdapter;
    TabLayout tabLayout;
    Button login;
    View view;
    LinearLayout profilepanel, notloggedin;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.content_main_profile, container, false);
        NestedScrollView scrollView = (NestedScrollView) view.findViewById(R.id.nest_scrollview);
        scrollView.setFillViewport(true);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) view.findViewById(R.id.tablayout);
        setupViewPager(viewPager);
        setuptabLayout();
        init();
        setData();
        return view;
    }

    public void setuptabLayout() {

        try {
            tabLayout.setupWithViewPager(viewPager);

            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = tabLayout.getTabAt(i);
                tab.setCustomView(pageAdapter.getTabView(i));
            }
            tabLayout.requestFocus();
            //      tabLayout.getTabAt(4).select();
        } catch (Exception ex) {
            Log.e("errr2", ex.getMessage());
        }

    }

    public void setupViewPager(ViewPager viewPager) {
        pageAdapter = new MyImagePagerAdapter(activity, getChildFragmentManager());
        pageAdapter.addFragment(new Setting(), R.drawable.setting);
        pageAdapter.addFragment(new PersonalData(), R.drawable.person);
        pageAdapter.addFragment(new profile(), R.drawable.people);
        viewPager.setAdapter(pageAdapter);

    }

    public void init() {
        profilepanel = (LinearLayout) view.findViewById(R.id.profilepanel);
        notloggedin = (LinearLayout) view.findViewById(R.id.notloggedin);
        login = (Button) view.findViewById(R.id.login);

    }

    public void setData() {

        if (Preferences.user == null) {
            profilepanel.setVisibility(View.GONE);
            notloggedin.setVisibility(View.VISIBLE);
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShowSignMethod();
                }
            });
        } else {
            profilepanel.setVisibility(View.VISIBLE);
            notloggedin.setVisibility(View.GONE);

        }
    }

}
