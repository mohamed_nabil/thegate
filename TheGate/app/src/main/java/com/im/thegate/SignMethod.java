package com.im.thegate;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import Model.NobelActivity;

public class SignMethod extends NobelActivity {


    ImageView loginface, loginemail;
    TextView Register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_method);
        init();


    }

    public void init() {

        loginface = (ImageView) findViewById(R.id.loginface);
        loginemail = (ImageView) findViewById(R.id.loginemail);
        Register = (TextView) findViewById(R.id.haveaccount);

        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SignMethod.this, Login.class);
                startActivity(i);
                finish();
            }
        });
        loginemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SignMethod.this, SignUp.class);
                startActivity(i);
                finish();
            }
        });


    }


    public void back(View V) {
        super.onBackPressed();
    }

}
