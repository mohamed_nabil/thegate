package com.im.thegate;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import Adapters.MyPagerAdapter;
import Model.NobelFragment;

/**
 * Created by Admin on 8/7/2017.
 */

public class MainTabsFragment extends NobelFragment {

    public static String Tag = "maintabs";
    View view;
    ViewPager viewPager;
    MyPagerAdapter pageAdapter;
    int CurrentTab = -1;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.maintabsview, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.mainViewPager);

        //   viewPager =(ViewPager)findViewById(R.id.mainviewpager);
        // tabLayout=(TabLayout) findViewById(R.id.maintablayout);
        //setupViewPager(viewPager);
        //setuptabLayout();
        setupViewPager(viewPager);
        return view;

    }

    public void setupViewPager(ViewPager viewPager) {
        pageAdapter = new MyPagerAdapter(activity, getChildFragmentManager());
        pageAdapter.addFragment(new StoreFragment());
        pageAdapter.addFragment(new NewsFragment());
        CopounFragment f = new CopounFragment();
        f.setBook(false);
        DiscountBookFragment f1 = new DiscountBookFragment();
        f1.setBook(true);
        pageAdapter.addFragment(f);
        pageAdapter.addFragment(f1);
        pageAdapter.addFragment(new ProfileMain());
        viewPager.setAdapter(pageAdapter);
        if (CurrentTab != -1) viewPager.setCurrentItem(CurrentTab, true);
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    public void setTab(int i) {
        CurrentTab = i;
    }

    public void changeTab(View view) {

        if (view.getId() == R.id.tab1) {
            viewPager.setCurrentItem(0, true);
        } else if (view.getId() == R.id.tab2) {
            viewPager.setCurrentItem(1, true);
        } else if (view.getId() == R.id.tab3) {
            viewPager.setCurrentItem(2, true);
        } else if (view.getId() == R.id.tab4) {
            viewPager.setCurrentItem(3, true);
        } else if (view.getId() == R.id.tab5) {
            viewPager.setCurrentItem(4, true);
        }

    }


}
