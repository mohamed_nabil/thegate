package com.im.thegate;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import Model.NobelActivity;

public class ShareActivity extends NobelActivity {


    RelativeLayout face, twitter, messanger, email, other, sms, whats;

    String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        link = getIntent().getStringExtra("link");


        init();
    }

    public void init() {

        face = (RelativeLayout) findViewById(R.id.face);
        twitter = (RelativeLayout) findViewById(R.id.twitter);
        messanger = (RelativeLayout) findViewById(R.id.messanger);
        email = (RelativeLayout) findViewById(R.id.Email);
        other = (RelativeLayout) findViewById(R.id.other);
        sms = (RelativeLayout) findViewById(R.id.SMS);
        whats = (RelativeLayout) findViewById(R.id.whatsapp);

        face.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String packname = "com.facebook.katana";
                if (!Share(packname)) {
                    String urlToShare = link;
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_SUBJECT, "The Gate"); // NB: has no effect!
                    intent.putExtra(Intent.EXTRA_TEXT, urlToShare);

                    String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + urlEncode(link);
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
                    startActivity(intent);
                }

            }
        });

        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String packname = "com.twitter.android";
                if (!Share(packname)) {
                    String urlToShare = link;
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_SUBJECT, "The Gate"); // NB: has no effect!
                    intent.putExtra(Intent.EXTRA_TEXT, urlToShare);

                    String sharerUrl = "https://twitter.com/intent/tweet?text=" + urlEncode(link);
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
                    startActivity(intent);
                }

            }
        });

        messanger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String packname = "com.facebook.orca";
                if (!Share(packname)) {
                    Toast.makeText(ShareActivity.this, "please Install Facebook Messanger", Toast.LENGTH_LONG).show();
                }

            }
        });
        whats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String packname = "com.whatsapp";
                if (!Share(packname)) {
                    Toast.makeText(ShareActivity.this, "please Install whats app Messanger", Toast.LENGTH_LONG).show();
                }

            }
        });
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "the Gate App");
                emailIntent.putExtra(Intent.EXTRA_TEXT, link);
//emailIntent.putExtra(Intent.EXTRA_HTML_TEXT, body); //If you are using HTML in your body text

                startActivity(Intent.createChooser(emailIntent, "the Gate"));

            }
        });
        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:"));
                sendIntent.putExtra("sms_body", link);
                startActivity(sendIntent);

            }
        });
        other.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        link);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);

            }
        });


    }


    public void back(View V) {
        super.onBackPressed();
    }

    public boolean Share(String pack_name) {
        String urlToShare = link;
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "The Gate"); // NB: has no effect!
        intent.putExtra(Intent.EXTRA_TEXT, urlToShare);

// See if official Facebook app is found
// As fallback, launch sharer.php in a browser

        boolean Found = false;
        List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith(pack_name)) {
                intent.setPackage(info.activityInfo.packageName);
                Found = true;
                break;
            }
        }

        if (Found) {
            startActivity(intent);
            return true;
        } else return false;
    }

    private String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.e("share", "UTF-8 should always be supported", e);
            return "";
        }
    }
}
