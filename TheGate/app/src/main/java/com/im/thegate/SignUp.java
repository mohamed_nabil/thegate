package com.im.thegate;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import java.util.ArrayList;

import Adapters.countrycodesAdapter;
import ConnectionHandler.JsonParser;
import ConnectionHandler.ServiceCallback;
import Model.NobelActivity;
import Model.User;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class SignUp extends NobelActivity implements View.OnClickListener {


    ArrayList<String> countrylist;
    ArrayList<String> codes;
    String UsernameString, EmailString, AgeString, genderString, PasswordString, PhoneString, CountryCodeString;
    private android.widget.EditText username;
    private android.widget.EditText email;
    private android.widget.Spinner countrycodespinner;
    private android.widget.EditText phone;
    private android.widget.RadioButton gendermale;
    private android.widget.RadioButton genderfemale;
    private android.widget.EditText age;
    private android.widget.EditText passsword;
    private android.widget.Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        init();
    }

    public void init() {

        this.register = (Button) findViewById(R.id.register);
        this.passsword = (EditText) findViewById(R.id.passsword);
        this.age = (EditText) findViewById(R.id.age);
        this.genderfemale = (RadioButton) findViewById(R.id.gender_female);
        this.gendermale = (RadioButton) findViewById(R.id.gender_male);
        this.phone = (EditText) findViewById(R.id.phone);
        this.countrycodespinner = (Spinner) findViewById(R.id.country_code_spinner);
        this.email = (EditText) findViewById(R.id.email);
        this.username = (EditText) findViewById(R.id.username);

        countrylist = Preferences.getCountriesCodeNames();
        codes = Preferences.getCountriesCode();

        countrycodesAdapter adapter = new countrycodesAdapter(activity, android.R.layout.simple_dropdown_item_1line, countrylist);
        countrycodespinner.setAdapter(adapter);

        register.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == register.getId()) {

            if (!ValidateUsername()) return;
            if (!ValidateEmail()) return;
            if (!ValidatePhone()) return;
            if (!ValidateGender()) return;
            if (!ValidateAge()) return;
            if (!ValidatePassword()) return;
            Preferences.connector.Register(GetLoadingDialogue(), EmailString, PasswordString, AgeString,
                    UsernameString, genderString, CountryCodeString, PhoneString, new ServiceCallback() {
                        @Override
                        public void onSuccess(String Response) {

                            Preferences.user = JsonParser.fromJsonString(Response, User.class);
                            ShowMessage("User Registered Successfully").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    finish();
                                }
                            });

                        }

                        @Override
                        public void onFail(String Message) {
                            ShowMessage(Message);

                        }
                    });

        }
    }

    public boolean ValidateUsername() {


        UsernameString = this.username.getText().toString();

        if (!UsernameString.equals("")) {
            return true;
        } else {
            ShowMessage("Invaild User name");
            return false;
        }
    }

    public boolean ValidateEmail() {


        EmailString = this.email.getText().toString();

        if (android.util.Patterns.EMAIL_ADDRESS.matcher(EmailString).matches()) {
            return true;
        } else {
            ShowMessage("Invaild Email");
            return false;
        }
    }

    public boolean ValidatePhone() {

        if (countrycodespinner.getSelectedItemPosition() == -1) {
            ShowMessage("please select country code");
            return false;

        } else {
            CountryCodeString = codes.get(countrycodespinner.getSelectedItemPosition());
        }

        PhoneString = this.phone.getText().toString();

        if (PhoneString.length() > 0) {
            return true;
        } else {
            ShowMessage("Invaild Phone");
            return false;
        }
    }

    public boolean ValidatePassword() {

        PasswordString = this.passsword.getText().toString();

        if (PasswordString.length() < 6) {
            ShowMessage("Password less than 6 chars");
            return false;
        }

        return true;
    }

    public boolean ValidateGender() {
        if (!genderfemale.isChecked() && !gendermale.isChecked()) {
            ShowMessage("Please Select gender");
            return false;

        }
        if (genderfemale.isChecked()) genderString = "female";
        else genderString = "male";

        return true;

    }


    public boolean ValidateAge() {
        AgeString = age.getText().toString();

        try {
            Integer.valueOf(AgeString);
            return true;
        } catch (NumberFormatException e) {
            ShowMessage("Invalide Age");
            return false;
        }
    }

    public void back(View V) {
        super.onBackPressed();
    }

}
