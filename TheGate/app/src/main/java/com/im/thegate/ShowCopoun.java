package com.im.thegate;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import Adapters.MyPagerAdapter;
import Model.NobelActivity;

public class ShowCopoun extends NobelActivity {

    ViewPager viewPager;
    MyPagerAdapter pageAdapter;
    TabLayout tabLayout;
    TextView title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_copoun);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        title = (TextView) findViewById(R.id.title);

        title.setText(Preferences.currentCopoun.getMerchant_Name());

        ImageView share = (ImageView) findViewById(R.id.share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shareactivity = new Intent(activity, ShareActivity.class);
                shareactivity.putExtra("link", "http://thegate.deals/en/deal_single.php?CoId=" + Preferences.currentCopoun.getId());
                startActivity(shareactivity);
            }
        });


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        setupViewPager(viewPager);
        setuptabLayout();


    }

    public void setuptabLayout() {

        tabLayout.setupWithViewPager(viewPager);
    }

    public void setupViewPager(ViewPager viewPager) {
        pageAdapter = new MyPagerAdapter(activity, getSupportFragmentManager());
        pageAdapter.addFragment(new CouponOfferFragment(), "Offer");
        pageAdapter.addFragment(new CouponDetailsFragment(), "Details");
        viewPager.setAdapter(pageAdapter);
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

    }

    public void back(View view) {
        super.onBackPressed();
    }

}
