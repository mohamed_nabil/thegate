package com.im.thegate;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import Adapters.StoresMapHorizintalAdapter;
import ConnectionHandler.ServiceCallback;
import Model.Coupon;
import Model.NobelFragment;
import Model.Store;
import Model.feature;
import MyWidget.HorizontalListView;

/**
 * Created by Admin on 8/7/2017.
 */

public class CouponDetailsFragment extends NobelFragment {


    View view;
    MapView mMapView;
    LinearLayout website, getdirections, featureLinear;
    TextView Details, before, after, curr;
    RelativeLayout footer;
    ImageView email, face, whats, viper, twitter, insta, videofooter;
    Coupon coupon;
    ArrayList<Store> AvailableStores;
    StoresMapHorizintalAdapter adapter;
    HorizontalListView storelist;
    Store CurrentStore;
    private GoogleMap googleMap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.copoun_details_fragment, container, false);

        mMapView = (MapView) view.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        InitializeMap();
        coupon = Preferences.currentCopoun;
        init();
        setData();
        PrepareSocialActions();
        getAvailableStores();


        return view;
    }

    public void getAvailableStores() {

        Preferences.connector.GetAvailableStoresbyCId(coupon.getId(), ShowLoadingDialogue(), new ServiceCallback() {
            @Override
            public void onSuccess(String Response) {

                AvailableStores = Preferences.jsonParser.fromJsonArray(Response, Store[].class);
                StoresLoaded();

            }
        });

    }

    public void StoresLoaded() {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                adapter = new StoresMapHorizintalAdapter(context, AvailableStores);
                storelist.setAdapter(adapter);

                if (AvailableStores.size() != 0) {
                    CurrentStore = AvailableStores.get(0);
                    StoreChanged();
                }
                storelist.registerListItemClickListener(new HorizontalListView.OnListItemClickListener() {
                    @Override
                    public void onClick(View v, int position) {
                        CurrentStore = AvailableStores.get(position);
                        StoreChanged();
                    }
                });

            }
        });

    }

    public void StoreChanged() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Details.setText(Html.fromHtml(CurrentStore.getDetails(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            Details.setText(Html.fromHtml(CurrentStore.getDetails()));

        }

        PrepareSocialActions();
        setFeatures();
        AddMarker();
    }

    public void setFeatures() {
        featureLinear = (LinearLayout) view.findViewById(R.id.featureLinear);
        final LayoutInflater mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        ArrayList<feature> features = CurrentStore.getFeatures();
        //Preferences.jsonParser.fromJsonArray(Response, feature[].class);

        featureLinear.removeAllViews();

        for (feature f : features) {
            View view = mLayoutInflater.inflate(R.layout.feature_item_linear, null);
            ImageView imageView = (ImageView) view.findViewById(R.id.feature_image);
            TextView textView = (TextView) view.findViewById(R.id.feature_text);
            Preferences.imageLoader.displayImage(Preferences.ImgPath + f.getIcon(), imageView);

            textView.setText(f.getName());

            featureLinear.addView(view);

        }

    }


    public void InitializeMap() {
        mMapView.onResume(); // needed to get the map to display immediately
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                //              googleMap.setMyLocationEnabled(true);
//                myMap.getUiSettings().setMyLocationButtonEnabled(true);

                // For dropping a marker at a point on the Map
            }
        });

    }

    public void AddMarker() {

        if (googleMap == null) return;

        if (CurrentStore != null) {

            double v1, v2;
            try {
                v1 = Double.valueOf(CurrentStore.getLat());
                v2 = Double.valueOf(CurrentStore.getLng());
            } catch (NumberFormatException e) {
                Toast.makeText(activity, "not Available", Toast.LENGTH_SHORT).show();
                return;
            }
            LatLng sydney = new LatLng(v1, v2);
            googleMap.addMarker(new MarkerOptions().position(sydney).title(CurrentStore.getMerchant_Name()).snippet(CurrentStore.getAddress()));

            // For zooming automatically to the location of the marker
            CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }

    }

    public void init() {
        Details = (TextView) view.findViewById(R.id.details);
        email = (ImageView) view.findViewById(R.id.email);
        face = (ImageView) view.findViewById(R.id.facebook);
        whats = (ImageView) view.findViewById(R.id.whats);
        viper = (ImageView) view.findViewById(R.id.viper);
        twitter = (ImageView) view.findViewById(R.id.twitter);
        insta = (ImageView) view.findViewById(R.id.insta);
        website = (LinearLayout) view.findViewById(R.id.website);
        getdirections = (LinearLayout) view.findViewById(R.id.showlocation);
        storelist = (HorizontalListView) view.findViewById(R.id.storelist);
        footer = (RelativeLayout) view.findViewById(R.id.footer);
        curr = (TextView) view.findViewById(R.id.curr);
        before = (TextView) view.findViewById(R.id.before);
        after = (TextView) view.findViewById(R.id.after);
        videofooter = (ImageView) view.findViewById(R.id.videofooter);


    }

    public void setData() {

        Details.setText(coupon.getCoupon_short_Details());
        if (coupon.getIs_book().equals("0")) {

            before.setText(coupon.getPrice_before());
            before.setPaintFlags(before.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            after.setText(coupon.getDeal_price());
            curr.setText(Preferences.country.getCurrency());

        } else {
            footer.setVisibility(View.GONE);

        }


    }

    public void PrepareSocialActions() {

        videofooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CurrentStore.getVideo_url() == null || CurrentStore.getVideo_url().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }
                Intent i = new Intent(activity, WebTube.class);
                i.putExtra("title", CurrentStore.getMerchant_Name());
                i.putExtra("video_url", CurrentStore.getVideo_url());
                startActivity(i);


            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CurrentStore.getEmail() == null || CurrentStore.getEmail().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{CurrentStore.getEmail()});
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(activity, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        face.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CurrentStore.getFb_url() == null || CurrentStore.getFb_url().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                String url = CurrentStore.getFb_url();
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);

            }
        });
        whats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CurrentStore.getWhatsapp() == null || CurrentStore.getWhatsapp().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                Uri uri = Uri.parse("smsto:" + CurrentStore.getWhatsapp());
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                i.setPackage("com.whatsapp");
                startActivity(Intent.createChooser(i, ""));

            }
        });

        viper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE);
                if (result == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + CurrentStore.getPhone()));
                    startActivity(intent);
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CALL_PHONE)) {
                    } else {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, 1);
                    }
                }


            }
        });
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CurrentStore.getTwitter_url() == null || CurrentStore.getTwitter_url().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                String url = CurrentStore.getTwitter_url();
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);


            }
        });
        insta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CurrentStore.getInstagram() == null || CurrentStore.getInstagram().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                String url = CurrentStore.getInstagram();
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);

            }
        });
        website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CurrentStore.getWebsite_url() == null || CurrentStore.getWebsite_url().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                String url = CurrentStore.getWebsite_url();
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        });
        getdirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CurrentStore.getLat().equals("") || CurrentStore.getLat().equals("0.0")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                show_location();

            }
        });


    }

    public void show_location() {
        // String uri = String.format(Locale.ENGLISH, "geo:%d,%d", latitude, longitude);
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr=" + CurrentStore.getLat() + "," + CurrentStore.getLng()));
        startActivity(intent);

    }


    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


    public void LoadMerchantDetails() {

    }


}
