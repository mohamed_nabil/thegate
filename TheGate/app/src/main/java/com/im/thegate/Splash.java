package com.im.thegate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import Adapters.CustomDialogCountry;
import Adapters.CustomDialogLang;
import ConnectionHandler.Connector;
import ConnectionHandler.JsonParser;
import ConnectionHandler.ServiceCallback;
import Model.Country;
import Model.NobelActivity;

public class Splash extends NobelActivity {
    Handler handler = new Handler();
    SharedPreferences sharedPreferences;
    String codes[] = new String[]{"en"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        OpenGate();
        //parser.Test();
        init();
        LoadCountry();
        loadEsentioalData();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                final CustomDialogLang cdd = new CustomDialogLang(Splash.this);
                cdd.AddOnItemClickListenr(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Intent intent = new Intent(Splash.this, MainHome.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("lang", codes[i]);
                        editor.apply();
                        cdd.dismiss();

                        CustomDialogCountry cdd = new CustomDialogCountry(Splash.this);
                        cdd.show();

                    }
                });

                cdd.show();

/*
                if (Preferences.lang==null||Preferences.lang.equals("-")){
                CustomDialogLang cdd = new CustomDialogLang(Splash.this);
                cdd.show();
                }
                else if (Preferences.country==null){
                    CustomDialogCountry cdd = new CustomDialogCountry(Splash.this);
                    cdd.show();

                }else {
                    startActivity(new Intent(Splash.this,MainHome.class));
                    finish();
                }
*/
            }
        }, 3000);


    }

    public void init() {
        sharedPreferences = getSharedPreferences("gate_file", Context.MODE_PRIVATE);
        Preferences.connector = Connector.getInstance(getApplicationContext(), "http://api.thegate.deals/api/");
        Preferences.jsonParser = JsonParser.getInstance();
        Preferences.lang = sharedPreferences.getString("lang", "en");
        InitializeImageLoader();
        Preferences.imageLoader = ImageLoader.getInstance();

    }

    public void loadEsentioalData() {

        if (Preferences.connector.isOnline() == true) {
            Thread thread = new Thread() {

                @Override
                public void run() {
                    Preferences.connector.LoadCountries(null, new ServiceCallback() {
                        @Override
                        public void onSuccess(String Response) {
                            Preferences.Countries = JsonParser.fromJsonArray(Response, Country[].class);

                        }
                    });
                }
            };
            thread.start();
        } else {

        }
    }

    public void LoadCountry() {
        String c_name = sharedPreferences.getString("country_name", "");
        String id = sharedPreferences.getString("country_id", "");
        String ci = sharedPreferences.getString("country_icon", "");
        String cr = sharedPreferences.getString("country_curr", "");

        if (id.equals(""))
            return;
        Preferences.country = new Country(id, c_name, ci, cr);
    }

    public void OpenGate() {
        ImageView left_gate = (ImageView) findViewById(R.id.left_gate);
        ImageView right_gate = (ImageView) findViewById(R.id.right_gate);
        left_gate.startAnimation(AnimationUtils.loadAnimation(this, R.anim.anim_slide_in_left));
        right_gate.startAnimation(AnimationUtils.loadAnimation(this, R.anim.anim_slide_in_right));

    }

    public void InitializeImageLoader() {
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                this)
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();
        ImageLoader.getInstance().init(config);


    }
}
