package com.im.thegate;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;

import Adapters.NewsListAdapter;
import ConnectionHandler.JsonParser;
import ConnectionHandler.ServiceCallback;
import Model.News;
import Model.NewsCategory;
import Model.NobelFragment;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Admin on 8/7/2017.
 */

public class NewsFragment extends NobelFragment {


    View view;
    RecyclerView recyclerView;
    NewsListAdapter adapter = null;
    ArrayList<News> News = null;
    TabLayout tabLayout;
    int page = 0;
    LinearLayoutManager linearLayoutManager;
    ProgressBar progressBar;
    boolean IsLoading = false;
    boolean nomoredata = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.news_main_fragment, container, false);

        tabLayout = (TabLayout) view.findViewById(R.id.tablayout);
        recyclerView = (RecyclerView) view.findViewById(R.id.RecyclerView);
        loadCategories();
        loadNews();
        progressBar = (ProgressBar) view.findViewById(R.id.progress);


        return view;
    }

    public void ShowProgress() {
        progressBar.setVisibility(View.VISIBLE);

    }

    public void HideProgress() {
        progressBar.setVisibility(View.GONE);

    }

    public void loadNews() {
        String catId = "";
        int catpos = tabLayout.getSelectedTabPosition();
        if (catpos > 0) catId = Preferences.NewsCategories.get(catpos - 1).getId();


        SweetAlertDialog dialog;
        if (page == 0)
            dialog = ShowLoadingDialogue();
        else dialog = null;


        Preferences.connector.loadNews(page, catId, dialog, new ServiceCallback() {
            @Override
            public void onSuccess(String Response) {


                ArrayList<News> temp = null;
                temp = Preferences.jsonParser.fromJsonArray(Response, News[].class);

                if (News == null)
                    News = new ArrayList<News>(30);

                if (temp == null || temp.size() == 0) {
                    nomoredata = true;
                }

                News.addAll(temp);


                if (page == 0) {

                    adapter = new NewsListAdapter(News);
                    linearLayoutManager = new LinearLayoutManager(activity);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(linearLayoutManager);

                    adapter.setListener(new NewsListAdapter.Listener() {
                        @Override
                        public void onClick(int position) {
                            Preferences.currentNews = News.get(position);
                            startActivity(new Intent(context, ShowNews.class));
                        }
                    });

                    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        int ydy = 0;

                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);

                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            if (IsLoading || nomoredata)
                                return;
                            int visibleItemCount = linearLayoutManager.getChildCount();
                            int totalItemCount = linearLayoutManager.getItemCount();
                            int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                            if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                                //End of list();
                                loadNews();
                                ShowProgress();
                                IsLoading = true;
                            }
                        }
                    });


                } else {
                    if (!nomoredata)
                        adapter.notifyDataSetChanged();
                }
                page++;
                IsLoading = false;
                HideProgress();

            }
        });

    }

    public void loadCategories() {

        if (Preferences.connector.isOnline() == true && Preferences.NewsCategories == null) {
            Thread thread = new Thread() {

                @Override
                public void run() {
                    Preferences.connector.loadNewsCategories(null, new ServiceCallback() {
                        @Override
                        public void onSuccess(String Response) {
                            Preferences.NewsCategories = JsonParser.fromJsonArray(Response, NewsCategory[].class);
                            LoadCategoriesTab();

                        }
                    });


                }

            };

            thread.start();
        } else {

            LoadCategoriesTab();
        }
    }

    public void LoadCategoriesTab() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (Preferences.lang.equals("ar"))
                    tabLayout.addTab(tabLayout.newTab().setText("الكل"));
                else tabLayout.addTab(tabLayout.newTab().setText("All"));

                for (NewsCategory c : Preferences.NewsCategories) {
                    tabLayout.addTab(tabLayout.newTab().setText(c.getName()));
                }
                tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        tabChanged();

                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });
                tabLayout.getTabAt(0).select();
            }
        });

    }

    public void tabChanged() {
        page = 0;
        nomoredata = false;

        if (News != null) {
            News.clear();
            adapter.notifyDataSetChanged();
        }

        loadNews();

    }
}
