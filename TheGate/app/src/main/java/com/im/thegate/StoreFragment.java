package com.im.thegate;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import Adapters.StoresListAdapter;
import ConnectionHandler.JsonParser;
import ConnectionHandler.ServiceCallback;
import Model.Category;
import Model.NobelFragment;
import Model.Store;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Admin on 8/7/2017.
 */
public class StoreFragment extends NobelFragment {

    public static String Tag = "storeFragment";
    Category category;
    RecyclerView recyclerView;
    ArrayList<Store> datastores;
    StoresListAdapter storesListAdapter;
    View view;
    int page = 0;
    TabLayout tabLayout;
    MapView mMapView;
    StoreFragment fragment;
    LinearLayoutManager linearLayoutManager;
    boolean IsLoading = false;
    ProgressBar progressBar;
    boolean nomoredata = false;
    private GoogleMap googleMap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.stores_fragment_view, container, false);
        tabLayout = (TabLayout) view.findViewById(R.id.tablayout);
        mMapView = (MapView) view.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        InitializeMap();
        fragment = this;
        page = 0;


        loadCategories();
        getCategory();
        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        recyclerView = (RecyclerView) view.findViewById(R.id.listView);

        return view;
    }

    public void ShowProgress() {
        progressBar.setVisibility(View.VISIBLE);

    }

    public void HideProgress() {
        progressBar.setVisibility(View.GONE);

    }

    public void InitializeMap() {

        mMapView.onResume(); // needed to get the map to display immediately
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                if (Preferences.gps != null) {
                    Location location = Preferences.gps.getLocation();
                    Log.e("3", "3");

                    if (location != null) {
                        Log.e("5", "4");

                        Log.e("lat_x", location.getLatitude() + "");
                        Log.e("long_y", location.getLongitude() + "");
                        LatLng sydney = new LatLng(location.getLatitude(), location.getLongitude());
                        Preferences.userlocation = sydney;


                    }
                }
                // For showing a move to my location button
                //              googleMap.setMyLocationEnabled(true);
//                myMap.getUiSettings().setMyLocationButtonEnabled(true);

                // For dropping a marker at a point on the Map
                if (Preferences.userlocation != null) {
                    googleMap.addMarker(new MarkerOptions().position(Preferences.userlocation).title("you are here"));
                    // For zooming automatically to the location of the marker
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(Preferences.userlocation).zoom(12).build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                } else {
                    Toast.makeText(activity, "please enable GPs", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void addMarker(LatLng place, String name, String add) {

        if (googleMap == null) return;

        googleMap.clear();

        googleMap.addMarker(new MarkerOptions().position(place).title(name).snippet(add));

        // For zooming automatically to the location of the marker
        CameraPosition cameraPosition = new CameraPosition.Builder().target(place).zoom(12).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    public void loadCategories() {

        if (Preferences.connector.isOnline() == true && Preferences.categories == null) {
            Thread thread = new Thread() {

                @Override
                public void run() {
                    Preferences.connector.loadCategories(null, new ServiceCallback() {
                        @Override
                        public void onSuccess(String Response) {
                            Preferences.categories = JsonParser.fromJsonArray(Response, Category[].class);
                            LoadCategoriesTab();

                        }
                    });


                }

            };

            thread.start();
        } else {

            LoadCategoriesTab();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        //  loadCategories();


    }

    public void LoadCategoriesTab() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (Preferences.lang.equals("ar"))
                    tabLayout.addTab(tabLayout.newTab().setText("الكل"));
                else tabLayout.addTab(tabLayout.newTab().setText("All"));

                for (Category c : Preferences.categories) {
                    tabLayout.addTab(tabLayout.newTab().setText(c.getName()));
                }
                tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        tabChanged();
                        tabLayout.setScrollPosition(tab.getPosition(), 0f, true);


                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });
            }
        });
        if (Preferences.CatPos == -1) {
            tabLayout.getTabAt(0).select();
        } else {
            tabLayout.getTabAt(Preferences.CatPos + 1).select();
            tabLayout.setScrollPosition(Preferences.CatPos + 1, 0f, true);
            Preferences.CatPos = -1;
        }
//        getCategory();
    }

    public void tabChanged() {
        page = 0;
        nomoredata = false;

        if (datastores != null) {
            datastores.clear();
            storesListAdapter.notifyDataSetChanged();
        }

        getCategory();


    }

    public void getCategory() {

        Log.e("get Category", "enter");
        String catId = "";
        int catpos = tabLayout.getSelectedTabPosition();
        if (catpos > 0) catId = Preferences.categories.get(catpos - 1).getId();

        SweetAlertDialog dialog;
        if (page == 0)
            dialog = ShowLoadingDialogue();
        else dialog = null;

        Preferences.connector.loadStores(page, catId, dialog, new ServiceCallback() {
            @Override
            public void onSuccess(String Response) {


                ArrayList<Store> temp = null;
                temp = Preferences.jsonParser.fromJsonArray(Response, Store[].class);

                if (datastores == null)
                    datastores = new ArrayList<Store>(30);

                if (temp == null || temp.size() == 0) {
                    nomoredata = true;
                }

                datastores.addAll(temp);


                if (page == 0) {
                    storesListAdapter = new StoresListAdapter(datastores, getContext(), fragment);
                    linearLayoutManager = new LinearLayoutManager(activity);
                    recyclerView.setAdapter(storesListAdapter);
                    recyclerView.setLayoutManager(linearLayoutManager);
                    //    recyclerView.addItemDecoration(new SimpleDividerItemDecoration(context));


                    storesListAdapter.setListener(new StoresListAdapter.Listener() {
                        @Override
                        public void onClick(int position) {
                            Preferences.currentStore = datastores.get(position);
                            context.startActivity(new Intent(context, ShowStore.class));
                        }
                    });


                    recyclerView.setAdapter(storesListAdapter);
                    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        int ydy = 0;

                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);

                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            if (IsLoading || nomoredata)
                                return;
                            int visibleItemCount = linearLayoutManager.getChildCount();
                            int totalItemCount = linearLayoutManager.getItemCount();
                            int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                            if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                                //End of list();
                                getCategory();
                                ShowProgress();
                                IsLoading = true;
                            }
                        }
                    });


                } else {
                    if (!nomoredata)
                        storesListAdapter.notifyDataSetChanged();

                }

                page++;
                IsLoading = false;
                HideProgress();


            }
        });
    }




}

