package com.im.thegate;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import ConnectionHandler.ServiceCallback;
import Model.NobelActivity;
import Model.User;

public class Login extends NobelActivity {


    ImageView loginface;
    Button login;
    TextView forgetpassword, Register;
    EditText email, password;
    ServiceCallback loginreturned;
    String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();


    }

    public void init() {

        loginface = (ImageView) findViewById(R.id.facebook);
        login = (Button) findViewById(R.id.login);
        forgetpassword = (TextView) findViewById(R.id.forget);
        Register = (TextView) findViewById(R.id.register);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);

        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Login.this, SignMethod.class);
                startActivity(i);
                finish();
            }
        });

        loginreturned = new ServiceCallback() {
            @Override
            public void onSuccess(String Response) {

                User user = Preferences.jsonParser.fromJsonString(Response, User.class);
                if (user == null) {

                    ShowMessage("Wrong Email or Password... please try Again");
                } else {

                    Toast.makeText(activity, "Logged in Successfuly", Toast.LENGTH_SHORT).show();
                    Preferences.user = user;
                    finish();
                }

            }
        };

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String emailtext = email.getText().toString();
                String Password = password.getText().toString();

                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailtext).matches()) {
                    ShowMessage("please Enter valid Email");
                    return;
                }
                if (Password.equals("")) {
                    ShowMessage("please Enter Password First");
                    return;
                }

                Preferences.connector.login(GetLoadingDialogue(), emailtext, Password, loginreturned);

            }
        });

    }


    public void back(View V) {
        super.onBackPressed();
    }

}
