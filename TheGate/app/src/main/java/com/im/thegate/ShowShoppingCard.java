package com.im.thegate;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import Adapters.ShoppingCardListAdapter;
import Model.NobelActivity;

public class ShowShoppingCard extends NobelActivity {


    RecyclerView ShoppingList;
    Button next;
    TextView totalprice, currency;
    ShoppingCardListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_card);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        init();
    }

    public void init() {
        ShoppingList = (RecyclerView) findViewById(R.id.RecyclerView);
        totalprice = (TextView) findViewById(R.id.total);
        currency = (TextView) findViewById(R.id.currency);
        next = (Button) findViewById(R.id.next);

        setData();
    }

    public void setData() {
        currency.setText(Preferences.country.getCurrency());

        adapter = new ShoppingCardListAdapter(activity);
        ShoppingList.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        ShoppingList.setLayoutManager(linearLayoutManager);
        adapter.setListener(new ShoppingCardListAdapter.Listener() {
            @Override
            public void onClick(int position) {
                totalprice.setText("" + adapter.getTotalPrice());
            }
        });

        totalprice.setText("" + adapter.getTotalPrice());
    }


    public void back(View view) {
        super.onBackPressed();
    }


}
