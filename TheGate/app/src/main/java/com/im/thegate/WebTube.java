package com.im.thegate;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;


public class WebTube extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    TextView title;
    String videoID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_webview);

        title = (TextView) findViewById(R.id.title);
        title.setText(getIntent().getStringExtra("title"));
        ImageView back = (ImageView) findViewById(R.id.arrb);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        //https://www.youtube.com/watch?v=<VIDEO_ID>
        //L8ZB8kK42Ww
        videoID = getIntent().getStringExtra("video_url");

        if (videoID.contains("https://www.youtube.com/watch?v="))
            videoID = videoID.replace("https://www.youtube.com/watch?v=", "");

        if (videoID.contains("http://www.youtube.com/watch?v="))
            videoID = videoID.replace("http://www.youtube.com/watch?v=", "");

        if (videoID.contains("https://www.youtube.com/embed/"))
            videoID = videoID.replace("https://www.youtube.com/embed/", "");

        if (videoID.contains("http://www.youtube.com/embed/"))
            videoID = videoID.replace("http://www.youtube.com/embed/", "");


        //videoID="L8ZB8kK42Ww";
        YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_view);
        youTubePlayerView.initialize(getResources().getString(R.string.google_maps_key), this);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // ignore orientation/keyboard change
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult result) {
        Toast.makeText(this, "Failed to initialize.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        if (null == player) return;

        // Start buffering
        if (!wasRestored) {
            player.cueVideo(videoID);

            player.play();
        }
    }

    public void back(View view) {
        super.onBackPressed();
    }

}
