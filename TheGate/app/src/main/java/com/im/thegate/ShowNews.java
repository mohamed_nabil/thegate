package com.im.thegate;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.viewpagerindicator.CirclePageIndicator;

import Adapters.ImagesPagerAdapter;
import Model.News;
import Model.NobelActivity;

public class ShowNews extends NobelActivity {


    TextView title;
    ViewPager ImagesPager;
    ImagesPagerAdapter adapter;
    TextView Details;
    News news;
    LinearLayout cinema, video, review, favorite, location, call;

    Activity activity;
    ScrollerThread scrollerThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        activity = this;
        news = Preferences.currentNews;
        ImageView share = (ImageView) findViewById(R.id.share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shareactivity = new Intent(activity, ShareActivity.class);
                shareactivity.putExtra("link", "http://thegate.deals/en/news_single.php?NID=" + news.getId());
                startActivity(shareactivity);
            }
        });

        init();
        setData();
        PrepareSocialActions();


    }

    public void init() {
        // placeName = (TextView) findViewById(R.id.p);
        ImagesPager = (ViewPager) findViewById(R.id.viewpager);
        Details = (TextView) findViewById(R.id.details);

        cinema = (LinearLayout) findViewById(R.id.cinema);
        video = (LinearLayout) findViewById(R.id.view);
        review = (LinearLayout) findViewById(R.id.review);
        location = (LinearLayout) findViewById(R.id.location);
        favorite = (LinearLayout) findViewById(R.id.fav);
        call = (LinearLayout) findViewById(R.id.call);

        title = (TextView) findViewById(R.id.title);


    }

    public void setData() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Details.setText(Html.fromHtml(news.getNews_Details(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            Details.setText(Html.fromHtml(news.getNews_Details()));

        }


        title.setText(news.getNews_Title());

        if ((news.getVideo_link() == null || news.getVideo_link().equals(""))) {
            call.setVisibility(View.VISIBLE);
            location.setVisibility(View.VISIBLE);

            cinema.setVisibility(View.GONE);
            video.setVisibility(View.GONE);


        } else {
            call.setVisibility(View.GONE);
            location.setVisibility(View.GONE);

            cinema.setVisibility(View.VISIBLE);
            video.setVisibility(View.VISIBLE);

        }


        adapter = new ImagesPagerAdapter(this, news.getImages());
        scrollerThread = new ScrollerThread(this, ImagesPager);
        ImagesPager.setAdapter(adapter);

        CirclePageIndicator circlePageIndicator = (CirclePageIndicator) findViewById(R.id.drawableIndicator);
        circlePageIndicator.setViewPager(ImagesPager);
    }

    public void PrepareSocialActions() {

        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (news.getVideo_link() == null || news.getVideo_link().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }
                Intent i = new Intent(activity, WebTube.class);
                i.putExtra("title", news.getNews_Title());
                i.putExtra("video_url", news.getVideo_link());
                startActivity(i);


            }
        });
        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (true) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

            }
        });
        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (true) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (news.getPhone() == null || news.getPhone().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE);
                if (result == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + news.getPhone()));
                    startActivity(intent);
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CALL_PHONE)) {
                    } else {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, 1);
                    }
                }
            }
        });

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (news.getLat().equals("") || news.getLat().equals("0.0")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                show_location();

            }
        });

    }

    public void show_location() {
        // String uri = String.format(Locale.ENGLISH, "geo:%d,%d", latitude, longitude);
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr=" + news.getLat() + "," + news.getLng()));
        startActivity(intent);

    }

    @Override
    public void onResume() {
        super.onResume();


    }

    public void back(View view) {
        super.onBackPressed();
    }

    public class ScrollerThread extends Thread {

        ViewPager pager;
        Activity activity;

        ScrollerThread(Activity activity, ViewPager pager) {

            this.activity = activity;
            this.pager = pager;
        }

        public void run() {
            while (isAlive()) {

                try {
                    sleep(2000);
                } catch (Exception e) {

                }
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int size = pager.getChildCount();
                        int current = pager.getCurrentItem();
                        pager.setCurrentItem(current + 1 % size, true);

                    }
                });

            }
        }
    }


}
