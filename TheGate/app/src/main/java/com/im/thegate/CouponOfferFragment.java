package com.im.thegate;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.viewpagerindicator.CirclePageIndicator;

import Adapters.ImagesPagerAdapter;
import Model.Coupon;
import Model.NobelFragment;
import Model.ShopCardItem;

/**
 * Created by Admin on 8/7/2017.
 */

public class CouponOfferFragment extends NobelFragment {

    View view;
    TextView before, after, curr;
    RelativeLayout footer;
    Coupon coupon;

    TextView copounName;
    ImageView PlaceLogo;
    TextView placecount, placecounttext;
    ViewPager ImagesPager;
    ImagesPagerAdapter adapter;
    TextView Details, MerchantName;
    TextView highlights;
    TextView ratesum, dealview;
    Button Buy;
    RatingBar ratebar;
    ImageView email, face, whats, viper, twitter, insta, videofooter;
    ScrollerThread scrollerThread;
    LinearLayout timeremain;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.copoun_offer_fragment, container, false);
        coupon = Preferences.currentCopoun;
        init();
        setData();
        PrepareSocialActions();

        return view;
    }

    public void init() {
        PlaceLogo = (ImageView) view.findViewById(R.id.placelogo);
        copounName = (TextView) view.findViewById(R.id.copoun_name);
        MerchantName = (TextView) view.findViewById(R.id.merchant_name);
        ImagesPager = (ViewPager) view.findViewById(R.id.viewpager);
        Details = (TextView) view.findViewById(R.id.details);
        highlights = (TextView) view.findViewById(R.id.highlights);
        email = (ImageView) view.findViewById(R.id.email);
        face = (ImageView) view.findViewById(R.id.facebook);
        whats = (ImageView) view.findViewById(R.id.whats);
        viper = (ImageView) view.findViewById(R.id.viper);
        twitter = (ImageView) view.findViewById(R.id.twitter);
        insta = (ImageView) view.findViewById(R.id.insta);
        placecount = (TextView) view.findViewById(R.id.placescount);
        placecounttext = (TextView) view.findViewById(R.id.locations);
        videofooter = (ImageView) view.findViewById(R.id.videofooter);
        footer = (RelativeLayout) view.findViewById(R.id.footer);
        curr = (TextView) view.findViewById(R.id.curr);
        before = (TextView) view.findViewById(R.id.before);
        after = (TextView) view.findViewById(R.id.after);
        ratesum = (TextView) view.findViewById(R.id.ratesum);
        ratebar = (RatingBar) view.findViewById(R.id.rate);
        dealview = (TextView) view.findViewById(R.id.dealview);
        timeremain = (LinearLayout) view.findViewById(R.id.timeremain);
        Buy = (Button) view.findViewById(R.id.Buy);


    }

    public void setData() {
        Preferences.imageLoader.displayImage(Preferences.ImgPath + coupon.getMerchant_logo(), PlaceLogo);
        int cities = coupon.getCitiesNames().size();
        placecount.setText(cities + "");
        copounName.setText(coupon.getCoupon_name());
        MerchantName.setText(coupon.getMerchant_Name());
        ratesum.setText(coupon.getRateduser_count() + " Ratings");
        dealview.setText(coupon.getDeal_view() + "+ viewed today");
        Buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowMessage("Item Added to your Shopping Card");
                ShopCardItem item = new ShopCardItem();
                item.Amount = 1;
                item.coupon = coupon;
                item.Total_price = Double.valueOf(coupon.getDeal_price());
                MainHome.ShopCardList.add(item);
            }
        });

        timeremain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowMessage("available until " + coupon.getLast_date().split(" ")[0]);
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            highlights.setText(Html.fromHtml(coupon.getTerms(), Html.FROM_HTML_MODE_COMPACT));
            Details.setText(Html.fromHtml(coupon.getCoupon_explain(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            highlights.setText(Html.fromHtml(coupon.getTerms()));
            Details.setText(Html.fromHtml(coupon.getCoupon_explain()));
        }


        if (coupon.getIs_book().equals("0")) {
            before.setText(coupon.getPrice_before());
            before.setPaintFlags(before.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            after.setText(coupon.getDeal_price());
            curr.setText(Preferences.country.getCurrency());
        } else {
            footer.setVisibility(View.GONE);

        }

        if (Preferences.lang.equals("ar")) {

            placecounttext.setText(cities + " أماكن");
        } else {
            placecounttext.setText(cities + " Locations");
        }

        adapter = new ImagesPagerAdapter(context, coupon.getImages());
        scrollerThread = new ScrollerThread(getActivity(), ImagesPager);
        ImagesPager.setAdapter(adapter);

        CirclePageIndicator circlePageIndicator = (CirclePageIndicator) view.findViewById(R.id.drawableIndicator);
        circlePageIndicator.setViewPager(ImagesPager);


    }

    public void PrepareSocialActions() {
        videofooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (coupon.getVideo_url() == null || coupon.getVideo_url().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }
                Intent i = new Intent(activity, WebTube.class);
                i.putExtra("title", coupon.getMerchant_Name());
                i.putExtra("video_url", coupon.getVideo_url());
                startActivity(i);


            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (coupon.getEmail() == null || coupon.getEmail().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{coupon.getEmail()});
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(activity, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        face.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (coupon.getFb_url() == null || coupon.getFb_url().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                String url = coupon.getFb_url();
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);

            }
        });
        whats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (coupon.getWhatsapp() == null || coupon.getWhatsapp().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                Uri uri = Uri.parse("smsto:" + coupon.getWhatsapp());
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                i.setPackage("com.whatsapp");
                startActivity(Intent.createChooser(i, ""));

            }
        });

        viper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE);
                if (result == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + coupon.getPhone()));
                    startActivity(intent);
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CALL_PHONE)) {
                    } else {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, 1);
                    }
                }


            }
        });
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (coupon.getTwitter_url() == null || coupon.getTwitter_url().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                String url = coupon.getTwitter_url();
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);


            }
        });
        insta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (coupon.getInstagram() == null || coupon.getInstagram().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                String url = coupon.getInstagram();
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public class ScrollerThread extends Thread {

        ViewPager pager;
        Activity activity;

        ScrollerThread(Activity activity, ViewPager pager) {

            this.activity = activity;
            this.pager = pager;
        }

        public void run() {
            while (isAlive()) {

                try {
                    sleep(2000);
                } catch (Exception e) {

                }
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int size = pager.getChildCount();
                        int current = pager.getCurrentItem();
                        pager.setCurrentItem(current + 1 % size, true);

                    }
                });

            }
        }
    }




}
