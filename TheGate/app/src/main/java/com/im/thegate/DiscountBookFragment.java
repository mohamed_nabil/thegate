package com.im.thegate;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import Adapters.CopounsListAdapter;
import ConnectionHandler.JsonParser;
import ConnectionHandler.ServiceCallback;
import Model.Category;
import Model.Coupon;
import Model.NobelFragment;

/**
 * Created by Admin on 8/7/2017.
 */

public class DiscountBookFragment extends NobelFragment {


    View view;
    RecyclerView recyclerView;
    CopounsListAdapter adapter;
    TabLayout tabLayout;
    boolean isBook;
    EditText searchText;
    int page = 0;
    ArrayList<Coupon> dataCoupons;
    String CityId = "-1";
    LinearLayoutManager linearLayoutManager;


    public void setBook(boolean isBook) {
        this.isBook = isBook;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (activity != null)
                ((MainHome) activity).ShowCities();
            if (!CityId.equals("-1"))
                Preferences.CityId = CityId;
            else Preferences.CityId = "-1";
        } else {
            //    ((MainHome)activity).HideCities();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (activity != null)
            ((MainHome) activity).HideCities();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.copouns_main_fragment, container, false);
        tabLayout = (TabLayout) view.findViewById(R.id.tablayout);
        recyclerView = (RecyclerView) view.findViewById(R.id.RecyclerView);
        loadCategories();
        if (!isBook) {
            HideSearch();
        }
        InitializeSearch();
        loadCopouns();

        return view;
    }

    public void InitializeSearch() {
        searchText = (EditText) view.findViewById(R.id.searchtext);
        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });
    }


    public void performSearch() {
        String Id = searchText.getText().toString();


        for (Coupon c : dataCoupons) {
            if (c.getId().equals(Id)) {

                Preferences.currentCopoun = c;
                startActivity(new Intent(context, ShowCopoun.class));
                return;
            }
        }
        ShowMessage("No Cards Found with this Id");


    }
    public void HideSearch() {
        RelativeLayout search = (RelativeLayout) view.findViewById(R.id.search);

        search.setVisibility(View.GONE);
    }

    public void loadCopouns() {
        String catId = "";
        int catpos = tabLayout.getSelectedTabPosition();
        if (catpos > 0) catId = Preferences.categories.get(catpos - 1).getId();

        Preferences.connector.loadBookCoupons(page, catId, Preferences.CityId, ShowLoadingDialogue(), new ServiceCallback() {
            @Override
            public void onSuccess(String Response) {


                Log.e("book", "enter");
                if (dataCoupons != null) {
                    page = 0;
                    dataCoupons.clear();
                    adapter.notifyDataSetChanged();
                }

                ArrayList<Coupon> temp = null;
                Log.e("book", Response);

                temp = Preferences.jsonParser.fromJsonArray(Response, Coupon[].class);

                if (dataCoupons == null)
                    dataCoupons = new ArrayList<Coupon>(30);

                dataCoupons.addAll(temp);


                if (page == 0) {

                    adapter = new CopounsListAdapter(dataCoupons, activity);
                    recyclerView.setAdapter(adapter);
                    linearLayoutManager = new LinearLayoutManager(activity);
                    recyclerView.setLayoutManager(linearLayoutManager);

                    adapter.setListener(new CopounsListAdapter.Listener() {
                        @Override
                        public void onClick(int position) {
                            Preferences.currentCopoun = dataCoupons.get(position);
                            startActivity(new Intent(context, ShowCopoun.class));
                        }
                    });


                    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        int ydy = 0;

                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);

                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                            if (mIsLoading)
                            //                              return;
                            int visibleItemCount = linearLayoutManager.getChildCount();
                            int totalItemCount = linearLayoutManager.getItemCount();
                            int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                            if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                                //End of list
                                Log.e("recycler view", "bottom");
                            }
                        }
                    });

                }

            }
        });

    }

    public void loadCategories() {

        if (Preferences.connector.isOnline() == true && Preferences.categories == null) {
            Thread thread = new Thread() {

                @Override
                public void run() {
                    Preferences.connector.loadCategories(null, new ServiceCallback() {
                        @Override
                        public void onSuccess(String Response) {
                            Preferences.categories = JsonParser.fromJsonArray(Response, Category[].class);
                            LoadCategoriesTab();

                        }
                    });


                }

            };

            thread.start();
        } else {
            LoadCategoriesTab();

        }
    }

    public void LoadCategoriesTab() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (Preferences.lang.equals("ar"))
                    tabLayout.addTab(tabLayout.newTab().setText("الكل"));
                else tabLayout.addTab(tabLayout.newTab().setText("All"));

                for (Category c : Preferences.categories) {
                    tabLayout.addTab(tabLayout.newTab().setText(c.getName()));
                }
                tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        tabLayout.setScrollPosition(tab.getPosition(), 0f, true);

                        tabChanged();

                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });
            }
        });
        tabLayout.getTabAt(0).select();

    }

    public void tabChanged() {

        loadCopouns();

    }


}
