package com.im.thegate;

import android.location.LocationManager;

import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import ConnectionHandler.Connector;
import ConnectionHandler.JsonParser;
import ConnectionHandler.ServiceCallback;
import Model.Category;
import Model.City;
import Model.Country;
import Model.Coupon;
import Model.GPS;
import Model.News;
import Model.NewsCategory;
import Model.Store;
import Model.User;

/**
 * Created by Admin on 8/7/2017.
 */

public class Preferences {
    public static String lang = "en";
    public static String ImgPath = "http://thegate.deals/img/media/";
    public static JsonParser jsonParser;
    public static Connector connector;
    public static ArrayList<Country> Countries;
    public static ArrayList<Category> categories;
    public static ArrayList<Coupon> homeCoupons;
    public static ArrayList<News> homeNews;
    public static ArrayList<NewsCategory> NewsCategories;
    public static Country country;
    public static ImageLoader imageLoader;
    public static int CatPos;
    public static ArrayList<City> Cities;
    public static Coupon currentCopoun;
    public static Store currentStore;
    public static News currentNews;
    public static String CityId = "-1";
    public static LocationManager locationManager;
    public static GPS gps;
    public static LatLng userlocation;
    public static User user;

    public static ArrayList<String> getCountries() {

        ArrayList<String> names = new ArrayList<>(Countries.size());
        for (Country c : Countries)
            names.add(c.getName());
        return names;
    }

    public static ArrayList<String> getCountriesCode() {

        ArrayList<String> names = new ArrayList<>(Countries.size());
        for (Country c : Countries)
            names.add(c.getCode());
        return names;
    }

    public static ArrayList<String> getCountriesCodeNames() {

        ArrayList<String> names = new ArrayList<>(Countries.size());
        for (Country c : Countries)
            names.add(c.getName() + " " + c.getCode());
        return names;
    }


    public static ArrayList<String> getCities() {
        if (Cities == null) {
            connector.LoadCities(null, new ServiceCallback() {
                @Override
                public void onSuccess(String Response) {
                    Cities = JsonParser.fromJsonArray(Response, City[].class);
                }
            });

            ArrayList<String> names = new ArrayList<>(Countries.size());
            for (City c : Cities)
                names.add(c.getName());
            return names;
        } else {
            ArrayList<String> names = new ArrayList<>(Countries.size());
            for (City c : Cities)
                names.add(c.getName());
            return names;

        }

    }


    public static void LoadEssentialData() {


        connector.loadNewsCategories(null, new ServiceCallback() {
            @Override
            public void onSuccess(String Response) {
                Preferences.NewsCategories = JsonParser.fromJsonArray(Response, NewsCategory[].class);

            }
        });
        connector.loadCategories(null, new ServiceCallback() {
            @Override
            public void onSuccess(String Response) {
                Preferences.categories = JsonParser.fromJsonArray(Response, Category[].class);

            }
        });
        connector.LoadCities(null, new ServiceCallback() {
            @Override
            public void onSuccess(String Response) {
                Cities = JsonParser.fromJsonArray(Response, City[].class);
            }
        });


    }


}
