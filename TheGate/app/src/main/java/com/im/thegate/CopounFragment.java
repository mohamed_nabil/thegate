package com.im.thegate;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import Adapters.CopounsListAdapter;
import ConnectionHandler.JsonParser;
import ConnectionHandler.ServiceCallback;
import Model.Category;
import Model.Coupon;
import Model.NobelFragment;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Admin on 8/7/2017.
 */

public class CopounFragment extends NobelFragment {


    View view;
    RecyclerView recyclerView;
    CopounsListAdapter adapter;
    TabLayout tabLayout;
    boolean isBook;
    int page = 0;
    ArrayList<Coupon> dataCoupons;
    String CityId = "-1";
    ProgressBar progressBar;
    LinearLayoutManager linearLayoutManager;
    boolean nomoredata = false, IsLoading = false;

    public void setBook(boolean isBook) {
        this.isBook = isBook;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            if (activity != null)
                ((MainHome) activity).ShowCities();

            if (!CityId.equals("-1"))
                Preferences.CityId = CityId;
            else Preferences.CityId = "-1";

        } else {
//            ((MainHome)activity).HideCities();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (activity != null)
            ((MainHome) activity).HideCities();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.copouns_main_fragment, container, false);
        tabLayout = (TabLayout) view.findViewById(R.id.tablayout);
        recyclerView = (RecyclerView) view.findViewById(R.id.RecyclerView);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);

        page = 0;
        loadCategories();
        if (!isBook) {
            HideSearch();
        }
        loadCopouns();

        return view;
    }

    public void HideSearch() {
        RelativeLayout search = (RelativeLayout) view.findViewById(R.id.search);

        search.setVisibility(View.GONE);
    }

    public void ShowProgress() {
        progressBar.setVisibility(View.VISIBLE);

    }

    public void HideProgress() {
        progressBar.setVisibility(View.GONE);

    }

    public void loadCopouns() {

        IsLoading = true;
        String catId = "";
        int catpos = tabLayout.getSelectedTabPosition();
        if (catpos > 0) catId = Preferences.categories.get(catpos - 1).getId();

        SweetAlertDialog dialog;
        if (page == 0)
            dialog = ShowLoadingDialogue();
        else dialog = null;


        Preferences.connector.loadCoupons(page, catId, Preferences.CityId, dialog, new ServiceCallback() {
            @Override
            public void onSuccess(String Response) {




                ArrayList<Coupon> temp = null;
                temp = Preferences.jsonParser.fromJsonArray(Response, Coupon[].class);

                if (dataCoupons == null)
                    dataCoupons = new ArrayList<Coupon>(30);

                if (temp == null || temp.size() == 0) {
                    nomoredata = true;
                }

                dataCoupons.addAll(temp);


                if (page == 0) {

                    adapter = new CopounsListAdapter(dataCoupons, activity);
                    linearLayoutManager = new LinearLayoutManager(activity);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(linearLayoutManager);

                    adapter.setListener(new CopounsListAdapter.Listener() {
                        @Override
                        public void onClick(int position) {
                            Preferences.currentCopoun = dataCoupons.get(position);
                            startActivity(new Intent(context, ShowCopoun.class));
                        }
                    });

                    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        int ydy = 0;

                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);

                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            if (IsLoading || nomoredata)
                                return;
                            int visibleItemCount = linearLayoutManager.getChildCount();
                            int totalItemCount = linearLayoutManager.getItemCount();
                            int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                            if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                                //End of list
                                loadCopouns();
                                ShowProgress();
                                IsLoading = true;
                            }
                        }
                    });

                } else {
                    if (!nomoredata)
                        adapter.notifyDataSetChanged();

                }

                page++;
                IsLoading = false;
                HideProgress();


            }
        });

    }

    public void loadCategories() {

        if (Preferences.connector.isOnline() == true && Preferences.categories == null) {
            Thread thread = new Thread() {

                @Override
                public void run() {
                    Preferences.connector.loadCategories(null, new ServiceCallback() {
                        @Override
                        public void onSuccess(String Response) {
                            Preferences.categories = JsonParser.fromJsonArray(Response, Category[].class);
                            LoadCategoriesTab();

                        }
                    });


                }

            };

            thread.start();
        } else {
            LoadCategoriesTab();

        }
    }

    public void LoadCategoriesTab() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (Preferences.lang.equals("ar"))
                    tabLayout.addTab(tabLayout.newTab().setText("الكل"));
                else tabLayout.addTab(tabLayout.newTab().setText("All"));

                for (Category c : Preferences.categories) {
                    tabLayout.addTab(tabLayout.newTab().setText(c.getName()));
                }
                tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        tabLayout.setScrollPosition(tab.getPosition(), 0f, true);
                        tabChanged();

                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });
            }
        });
        tabLayout.getTabAt(0).select();

    }

    public void tabChanged() {
        page = 0;
        nomoredata = false;
        if (dataCoupons != null) {
            dataCoupons.clear();
            adapter.notifyDataSetChanged();
        }

        loadCopouns();

    }
}
