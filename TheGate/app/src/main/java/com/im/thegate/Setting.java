package com.im.thegate;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import Model.NobelFragment;

/**
 * Created by Admin on 8/7/2017.
 */

public class Setting extends NobelFragment {

    View view;
    LinearLayout fav_linear;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.setting, container, false);


        init();
        setData();
        return view;
    }

    public void init() {
        fav_linear = (LinearLayout) view.findViewById(R.id.fav_linear);
    }

    public void setData() {
        if (Preferences.user == null) {
            fav_linear.setVisibility(View.GONE);
        }
    }
}
