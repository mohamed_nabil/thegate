package com.im.thegate;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import Adapters.ImagesPagerAdapter;
import Model.NobelActivity;
import Model.Store;
import Model.feature;

public class ShowStore extends NobelActivity {


    TextView placeName, title;
    ImageView PlaceLogo, email, face, whats, viper, twitter, insta, videofooter;
    ViewPager ImagesPager;
    ImagesPagerAdapter adapter;
    TextView Details;
    Store store;
    LinearLayout featureLinear, website, deals, getdirections;

    Activity activity;
    ScrollerThread scrollerThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_store);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        activity = this;
        store = Preferences.currentStore;
        init();
        ImageView share = (ImageView) findViewById(R.id.share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shareactivity = new Intent(activity, ShareActivity.class);
                shareactivity.putExtra("link", "http://thegate.deals/en/merchant_single.php?MerId=" + store.getMerchent_id());
                startActivity(shareactivity);
            }
        });

        setData();
        setFeatures();
        PrepareSocialActions();


    }

    public void init() {
        PlaceLogo = (ImageView) findViewById(R.id.placelogo);
        // placeName = (TextView) findViewById(R.id.p);
        ImagesPager = (ViewPager) findViewById(R.id.viewpager);
        Details = (TextView) findViewById(R.id.details);
        email = (ImageView) findViewById(R.id.email);
        face = (ImageView) findViewById(R.id.facebook);
        whats = (ImageView) findViewById(R.id.whats);
        viper = (ImageView) findViewById(R.id.viper);
        twitter = (ImageView) findViewById(R.id.twitter);
        insta = (ImageView) findViewById(R.id.insta);
        website = (LinearLayout) findViewById(R.id.website);
        getdirections = (LinearLayout) findViewById(R.id.showlocation);
        deals = (LinearLayout) findViewById(R.id.deals);
        title = (TextView) findViewById(R.id.title);
        videofooter = (ImageView) findViewById(R.id.videofooter);


    }

    public void setData() {
        Preferences.imageLoader.displayImage(Preferences.ImgPath + store.getMerchant_logo(), PlaceLogo);
        //   placeName.setText(store.getMerchant_Name_Arabic());
        Details.setText(store.getDetails());
        title.setText(store.getMerchant_Name());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Details.setText(Html.fromHtml(store.getDetails(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            Details.setText(Html.fromHtml(store.getDetails()));

        }


        adapter = new ImagesPagerAdapter(this, store.getImages());
        scrollerThread = new ScrollerThread(this, ImagesPager);
        ImagesPager.setAdapter(adapter);

        CirclePageIndicator circlePageIndicator = (CirclePageIndicator) findViewById(R.id.drawableIndicator);
        circlePageIndicator.setViewPager(ImagesPager);
    }

    public void PrepareSocialActions() {

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (store.getEmail() == null || store.getEmail().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{store.getEmail()});
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(activity, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        videofooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (store.getVideo_url() == null || store.getVideo_url().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }
                Intent i = new Intent(activity, WebTube.class);
                i.putExtra("title", store.getMerchant_Name());
                i.putExtra("video_url", store.getVideo_url());
                startActivity(i);


            }
        });

        face.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (store.getFb_url() == null || store.getFb_url().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                String url = store.getFb_url();
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);

            }
        });
        whats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (store.getWhatsapp() == null || store.getWhatsapp().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                Uri uri = Uri.parse("smsto:" + store.getWhatsapp());
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                i.setPackage("com.whatsapp");
                startActivity(Intent.createChooser(i, ""));

            }
        });

        viper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE);
                if (result == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + store.getPhone()));
                    startActivity(intent);
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CALL_PHONE)) {
                    } else {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, 1);
                    }
                }


            }
        });
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (store.getTwitter_url() == null || store.getTwitter_url().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                String url = store.getTwitter_url();
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);


            }
        });
        insta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (store.getInstagram() == null || store.getInstagram().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                String url = store.getInstagram();
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);

            }
        });
        website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (store.getWebsite_url() == null || store.getWebsite_url().equals("")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                String url = store.getWebsite_url();
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        });
        getdirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (store.getLat().equals("") || store.getLat().equals("0.0")) {
                    if (Preferences.lang.equals("ar"))
                        Toast.makeText(activity, "غير متوفر", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(activity, "Not Available", Toast.LENGTH_SHORT).show();

                    return;
                }

                show_location();

            }
        });

    }

    public void show_location() {
        // String uri = String.format(Locale.ENGLISH, "geo:%d,%d", latitude, longitude);
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr=" + store.getLat() + "," + store.getLng()));
        startActivity(intent);

    }

    @Override
    public void onResume() {
        super.onResume();


    }

    public void setFeatures() {
        featureLinear = (LinearLayout) findViewById(R.id.featureLinear);
        final LayoutInflater mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ArrayList<feature> features = store.getFeatures();
        //Preferences.jsonParser.fromJsonArray(Response, feature[].class);

        for (feature f : features) {
            View view = mLayoutInflater.inflate(R.layout.feature_item_linear, null);
            ImageView imageView = (ImageView) view.findViewById(R.id.feature_image);
            TextView textView = (TextView) view.findViewById(R.id.feature_text);
            Preferences.imageLoader.displayImage(Preferences.ImgPath + f.getIcon(), imageView);

            textView.setText(f.getName());

            featureLinear.addView(view);

        }


    }

    public void back(View view) {
        super.onBackPressed();
    }

    public class ScrollerThread extends Thread {

        ViewPager pager;
        Activity activity;

        ScrollerThread(Activity activity, ViewPager pager) {

            this.activity = activity;
            this.pager = pager;
        }

        public void run() {
            while (isAlive()) {

                try {
                    sleep(2000);
                } catch (Exception e) {

                }
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int size = pager.getChildCount();
                        int current = pager.getCurrentItem();
                        pager.setCurrentItem(current + 1 % size, true);

                    }
                });

            }
        }
    }


}
