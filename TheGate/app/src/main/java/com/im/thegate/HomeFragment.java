package com.im.thegate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import Adapters.CategoryHorizintalAdapter;
import Adapters.CouponHorizintalAdapter;
import Adapters.NewsHorizintalAdapter;
import ConnectionHandler.JsonParser;
import ConnectionHandler.ServiceCallback;
import Model.Category;
import Model.Coupon;
import Model.News;
import Model.NobelFragment;
import Model.book;
import MyWidget.HorizontalListView;

/**
 * Created by Admin on 8/7/2017.
 */

public class HomeFragment extends NobelFragment {

    public static int HomeFragmentID = 1001;
    public static String Tag = "homeFragment";
    View view;
    Fragment CurrentFragment;
    HorizontalListView latestcoupons;
    HorizontalListView LatestNews;
    HorizontalListView Categories;
    CategoryHorizintalAdapter categoryAdapter;
    CouponHorizintalAdapter CouponAdapter;
    NewsHorizintalAdapter newsHorizintalAdapter;
    ImageView book_image;
    book homebook;
    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.content_home, container, false);
        Categories = (HorizontalListView) view.findViewById(R.id.categories);
        latestcoupons = (HorizontalListView) view.findViewById(R.id.latestcoupons);
        LatestNews = (HorizontalListView) view.findViewById(R.id.latestnews);

        loadCategories();
        LoadHomeBook();
        loadCoupons();
        loadNews();


        return view;
    }

    public void LoadHomeBook() {
        book_image = (ImageView) view.findViewById(R.id.book_image);
        book_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainTabsFragment f = new MainTabsFragment();
                f.setTab(3);
                ((MainHome) activity).setFragment(f, MainTabsFragment.Tag);


            }
        });


        Preferences.connector.getHomeBook(null, new ServiceCallback() {
            @Override
            public void onSuccess(String Response) {

                homebook = Preferences.jsonParser.fromJsonString(Response, book.class);
                Preferences.imageLoader.displayImage(Preferences.ImgPath + homebook.getBook_image(), book_image);


            }
        });

    }

    public void CategoriesLoaded() {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                categoryAdapter = new CategoryHorizintalAdapter(context);
                Categories.setAdapter(categoryAdapter);
                Categories.registerListItemClickListener(new HorizontalListView.OnListItemClickListener() {
                    @Override
                    public void onClick(View v, int position) {

                        MainTabsFragment f = new MainTabsFragment();
                        f.setTab(0);
                        Preferences.CatPos = position;
                        ((MainHome) activity).setFragment(f, MainTabsFragment.Tag);

                    }
                });
            }
        });

    }

    public void CopounsLoaded() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                CouponAdapter = new CouponHorizintalAdapter(context, Preferences.homeCoupons);
                latestcoupons.setAdapter(CouponAdapter);
                latestcoupons.registerListItemClickListener(new HorizontalListView.OnListItemClickListener() {
                    @Override
                    public void onClick(View v, int position) {
                        if (position == 0) {
                            MainTabsFragment f = new MainTabsFragment();
                            f.setTab(2);
                            ((MainHome) activity).setFragment(f, MainTabsFragment.Tag);
                        } else {
                            Preferences.currentCopoun = Preferences.homeCoupons.get(position - 1);
                            startActivity(new Intent(activity, ShowCopoun.class));

                        }
                    }
                });

            }
        });
    }

    public void NewsLoaded() {
        Log.e("news", Preferences.homeNews.size() + "");
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                newsHorizintalAdapter = new NewsHorizintalAdapter(context, Preferences.homeNews);
                LatestNews.setAdapter(newsHorizintalAdapter);
                LatestNews.registerListItemClickListener(new HorizontalListView.OnListItemClickListener() {
                    @Override
                    public void onClick(View v, int position) {
                        if (position == 0) {
                            MainTabsFragment f = new MainTabsFragment();
                            f.setTab(1);
                            ((MainHome) activity).setFragment(f, MainTabsFragment.Tag);

                        } else {
                            Preferences.currentNews = Preferences.homeNews.get(position - 1);
                            startActivity(new Intent(context, ShowNews.class));

                        }
                    }
                });

            }
        });
    }

    public void loadCategories() {

        if (Preferences.categories == null) {
            Preferences.connector.loadCategories(null, new ServiceCallback() {
                @Override
                public void onSuccess(String Response) {
                    Preferences.categories = JsonParser.fromJsonArray(Response, Category[].class);
                    CategoriesLoaded();
                }
            });
        } else {
            CategoriesLoaded();
        }
    }

    public void loadCoupons() {
        if (Preferences.homeCoupons == null) {
            Preferences.connector.loadHomeCoupons(null, new ServiceCallback() {
                @Override
                public void onSuccess(String Response) {
                    Preferences.homeCoupons = JsonParser.fromJsonArray(Response, Coupon[].class);
                    CopounsLoaded();
                }
            });
        } else {
            CopounsLoaded();
        }
    }

    public void loadNews() {
        if (Preferences.homeNews == null) {
            Preferences.connector.loadHomeNews(null, new ServiceCallback() {
                @Override
                public void onSuccess(String Response) {
                    Preferences.homeNews = JsonParser.fromJsonArray(Response, News[].class);
                    NewsLoaded();
                }
            });
        } else {
            NewsLoaded();
        }
    }




}
