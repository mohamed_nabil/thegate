package com.im.thegate;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;

import Adapters.DrawerAdapter;
import Adapters.MySpinnerAdapter;
import ConnectionHandler.JsonParser;
import ConnectionHandler.ServiceCallback;
import Model.GPS;
import Model.NobelActivity;
import Model.ShopCardItem;

public class MainHome extends NobelActivity implements AdapterView.OnItemClickListener {

    public static ArrayList<ShopCardItem> ShopCardList = new ArrayList<>();
    ListView navList;
    Activity activity;
    Toolbar toolbar;
    DrawerLayout drawer;
    Spinner Country, City;
    Fragment CurrentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setDrawer();
        activity = this;

        init();

        setFragment(new HomeFragment(), HomeFragment.Tag);

        if (checkPermission()) {

            PermissionAllowed();

            Log.e("permission", "granetd");
        } else {
            Log.e("permission", "requesting");
            requestPermission();
        }

    }

    public void init() {

        ImageView Shopping = (ImageView) findViewById(R.id.shop_card);

        Shopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (MainHome.ShopCardList == null || MainHome.ShopCardList.size() == 0) {
                    ShowMessage("You have no items in Shopping Card");
                } else {
                    startActivity(new Intent(activity, ShowShoppingCard.class));
                }
            }
        });


    }

    public void PermissionAllowed() {
        Preferences.locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        Preferences.gps = new GPS(Preferences.locationManager, this);
        Log.e("2", "2");
        Location location = Preferences.gps.getLocation();
        Log.e("3", "3");

        if (location != null) {
            Log.e("5", "4");

            Log.e("lat_x", location.getLatitude() + "");
            Log.e("long_y", location.getLongitude() + "");
            LatLng sydney = new LatLng(location.getLatitude(), location.getLongitude());
            Preferences.userlocation = sydney;


        }
    }

    protected void setFragment(Fragment fragment, String tag) {

        FragmentManager mFragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.addToBackStack(tag);
        ft.add(R.id.mainframe, fragment, tag);
//        ft.add("your container id", fragment);
        ft.commit();
        CurrentFragment = fragment;
    }

    public void setDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Preferences.lang.equals("ar")) {
                    if (drawer.isDrawerOpen(Gravity.RIGHT)) {
                        drawer.closeDrawer(Gravity.RIGHT);
                    } else {
                        drawer.openDrawer(Gravity.RIGHT);
                    }
                } else {

                    if (drawer.isDrawerOpen(Gravity.LEFT)) {
                        drawer.closeDrawer(Gravity.LEFT);
                    } else {
                        drawer.openDrawer(Gravity.LEFT);
                    }

                }
            }
        });

        updateList();
        SetSpinner();
    }

    public void SetSpinner() {
        Country = (Spinner) findViewById(R.id.CountrySpinner);
        City = (Spinner) findViewById(R.id.CitySpinner);

        if (Preferences.Countries != null) {

            Thread thread = new Thread() {
                @Override
                public void run() {
                    super.run();

                    if (Preferences.Cities == null) {
                        Preferences.connector.LoadCities(null, new ServiceCallback() {
                            @Override
                            public void onSuccess(String Response) {

                                Preferences.Cities = JsonParser.fromJsonArray(Response, Model.City[].class);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        final ArrayList<String> countries = Preferences.getCountries();
                                        final ArrayList<String> cities = Preferences.getCities();
                                        cities.add(0, "all");

                                        if (countries != null) {
                                            MySpinnerAdapter adapter = new MySpinnerAdapter(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, countries);
                                            Country.setAdapter(adapter);
                                        }
                                        if (cities != null) {
                                            MySpinnerAdapter adapter = new MySpinnerAdapter(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, cities);
                                            City.setAdapter(adapter);

                                            Preferences.CityId = "-1";
                                            City.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                @Override
                                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                                                    if (i == 0) Preferences.CityId = "-1";
                                                    else
                                                        Preferences.CityId = Preferences.Cities.get(i - 1).getId();
                                                }

                                                @Override
                                                public void onNothingSelected(AdapterView<?> adapterView) {

                                                }
                                            });
                                        }
                                    }
                                });

                            }
                        });
                    }


                }
            };

            thread.start();
        }


    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;

        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            Log.e("permission", "Allowed");

            Toast.makeText(activity, "Read Sms Allowed.", Toast.LENGTH_LONG).show();

        } else {

            Log.e("permission", "requesting 2");

            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}
                    , 200);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 200:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("permission", " result granetd");

                    Toast.makeText(activity, "Permission Granted.", Toast.LENGTH_LONG).show();
                    PermissionAllowed();


                } else {
                    Log.e("permission", "result granetd");

                    Toast.makeText(activity, "Permission Denied, We Cannot Access your Location.", Toast.LENGTH_LONG).show();


                }
                break;
        }
    }

    public void ShowCities() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Country.setVisibility(View.GONE);
                City.setVisibility(View.VISIBLE);
                Log.e("city", "show");

            }
        });
    }

    public void HideCities() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                City.setVisibility(View.GONE);
                Country.setVisibility(View.VISIBLE);


                Log.e("city", "show");
            }
        });
    }

    private void updateList() {

        InitializeImageLoader();
        navList = (ListView) findViewById(R.id.navberlist);
        LayoutAnimationController controller
                = AnimationUtils.loadLayoutAnimation(
                getApplication(), R.anim.list_layout_controller);
        navList.setLayoutAnimation(controller);

        ArrayList<String> lheaders = new ArrayList<>();
        ArrayList<Integer> Icons = new ArrayList<>();

        if (Preferences.lang.equals("ar")) {
            // lheaders.add("الملف الشخصي");
            //Icons.add(R.drawable.profile);
            lheaders.add("أماكن");
            Icons.add(R.drawable.stores);
            lheaders.add("أخر الأخبار");
            Icons.add(R.drawable.news);
            lheaders.add("كوبونات الخصم");
            Icons.add(R.drawable.discountcard);
            lheaders.add("كتاب الخصومات");
            Icons.add(R.drawable.book);


        } else {
//            lheaders.add("Profile");
            //          Icons.add(R.drawable.profile);
            lheaders.add("store");
            Icons.add(R.drawable.stores);
            lheaders.add("news");
            Icons.add(R.drawable.news);
            lheaders.add("discount card");
            Icons.add(R.drawable.discountcard);
            lheaders.add("discount book");
            Icons.add(R.drawable.discountcard);


        }


       /* NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        TextView text = (TextView) navigationView.findViewById(R.id.nav_header_text);
        ImageView user_image=(ImageView) navigationView.findViewById(R.id.);
        ImageView logo=(ImageView) navigationView.findViewById(R.id.side_logo);

        if (Preferences.user!=null){
            text.setVisibility(View.VISIBLE);
            logo.setVisibility(View.GONE);
            user_image.setVisibility(View.VISIBLE);
            text.setText(Preferences.user.username);
            // ImageLoader imageLoader=ImageLoader.getInstance();
            //         imageLoader.displayImage(Preferences.PhotoDirectory+Preferences.user.getPhotoUrl(),user_image);
        }
        else {
            logo.setVisibility(View.VISIBLE);
            text.setVisibility(View.GONE);
            user_image.setVisibility(View.GONE);
            //              text.setText("PassPort");
//                user_image.setImageResource(R.drawable.member);

        }*/

        DrawerAdapter adapter = new DrawerAdapter(lheaders, getLayoutInflater(), Icons);
        navList.setAdapter(adapter);
        navList.setOnItemClickListener(this);


    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        MainTabsFragment myFragment = (MainTabsFragment) getSupportFragmentManager().findFragmentByTag(MainTabsFragment.Tag);
        if (myFragment != null && myFragment.isVisible()) {
            // add your code here
            myFragment.setTab(i);
        } else {
            MainTabsFragment f = new MainTabsFragment();
            f.setTab(i);
            setFragment(f, MainTabsFragment.Tag);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (Preferences.lang.equals("en"))
            if (drawer.isDrawerOpen(Gravity.LEFT)) {
                drawer.closeDrawer(Gravity.LEFT);
            } else {
                if (drawer.isDrawerOpen(Gravity.RIGHT)) {
                    drawer.closeDrawer(Gravity.RIGHT);

                }

            }
    }


    public void InitializeImageLoader() {
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .showImageForEmptyUri(R.mipmap.ic_launcher)
                .showImageOnFail(R.mipmap.ic_launcher)
                .displayer(new FadeInBitmapDisplayer(300)).build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                this)
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();
        ImageLoader.getInstance().init(config);


    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (Preferences.lang.equals("en") && drawer.isDrawerOpen(Gravity.LEFT)) {
                drawer.closeDrawer(Gravity.LEFT);

        } else if (Preferences.lang.equals("ar") && drawer.isDrawerOpen(Gravity.RIGHT)) {
                drawer.closeDrawer(Gravity.RIGHT);
        } else {

            FragmentManager fm = getSupportFragmentManager();
            if (fm.getBackStackEntryCount() > 1) {
                Log.e("MainActivity", "popping backstack");
                fm.popBackStack();
            } else {
                finish();
                Log.e("MainActivity", "nothing on backstack, calling super");
            }
        }

    }

    public void changeTab(View view) {
        MainTabsFragment myFragment = (MainTabsFragment) getSupportFragmentManager().findFragmentByTag(MainTabsFragment.Tag);
        if (myFragment != null && myFragment.isVisible()) {
            // add your code here
            myFragment.changeTab(view);
        }

    }

}
