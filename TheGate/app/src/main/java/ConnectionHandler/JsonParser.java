package ConnectionHandler;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

import Model.Country;
import Model.Store;
import Model.feature;

/**
 * Created by it on 1/11/2017.
 */
public class JsonParser {

    private static JsonParser Parser;
    private static Gson gson;
    String Test = "[{\"id\":\"5\",\"name\":\"Egypt\",\"arabicname\":\"مصر\"},{\"id\":\"9\",\"name\":\"QATAR\",\"arabicname\":\"قطر\"}]";
    String Test1 = "{\"id\":\"5\",\"name\":\"Egypt\",\"arabicname\":\"مصر\"}";

    public static JsonParser getInstance() {
        if (Parser == null) {
            Parser = new JsonParser();
            gson = new Gson();
            return Parser;
        }
        return Parser;

    }

    public static HashMap<String, String> ParseJsonObject(String out) {

        HashMap<String, String> map = new HashMap<>();
        try {
            JSONObject object = new JSONObject(out);
            Iterator<String> Keys = object.keys();
            String S = Keys.next();
            while (Keys.hasNext()) {
                S = Keys.next();
                map.put(S, object.getString(S));
            }

        } catch (Exception e) {
            Log.e("error ", "parsing json object");
            return null;
        }
        return map;
    }

    public static HashMap<String, String> ParseJsonObject(JSONObject object) {

        HashMap<String, String> map = new HashMap<>();
        Iterator<String> Keys = object.keys();
        String S;
        while (Keys.hasNext()) {
            S = Keys.next();
            try {
                Log.e(S, object.get(S).toString());
                map.put(S, object.get(S).toString());
            } catch (Exception e) {
                Log.e("errorparsingobject", S);
            }
        }

        return map;
    }

    public static ArrayList<HashMap<String, String>> ParseJsonArray(String out) {

        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
        try {

            JSONArray jsonArray = new JSONArray(out);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                HashMap<String, String> map = new HashMap<>();

                Iterator<String> Keys = object.keys();
                String S = "";
                while (Keys.hasNext()) {
                    S = Keys.next();
                    map.put(S, object.get(S).toString());
                }
                arrayList.add(map);
            }

        } catch (Exception e) {
            return null;
        }
        return arrayList;
    }

    public static <T> T fromJsonString(String Json, Type TypeOfObject) {
        T ob = null;
        if (Json == null || Json.equals("")) return ob;

        if (Json.equals("{")) return ob;
        return gson.fromJson(Json, TypeOfObject);

    }

    public static <T> ArrayList<T> fromJsonArray(String json, Class<T[]> clazz) {

        if (json == null || json.equals("")) return new ArrayList<T>();
        if (json.charAt(0) != '[') return new ArrayList<>();
        Log.e("json", json);
        T[] jsonToObject = gson.fromJson(json, clazz);
        return new ArrayList<T>(Arrays.asList(jsonToObject));


    }


    public static ArrayList<Store> ParseStoreArray(String Json) {

        ArrayList<Store> Stores = new ArrayList<>(30);
        try {
            JSONArray data = new JSONArray(Json);
            for (int i = 0; i < data.length(); i++) {

                Store s = ParseStore(data.get(i).toString());
                Stores.add(s);
            }
            return Stores;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static Store ParseStore(String Json) {
        try {
            Log.e("Json object", Json);
            JSONObject object = new JSONObject(Json);
            String features = object.get("features").toString();
            Log.e("features", features);
            object.remove("features");

            ArrayList<feature> arr = fromJsonArray(features, feature[].class);
            Store s = gson.fromJson(object.toString(), Store.class);
            s.setFeatures(arr);
            return s;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void Test() {
        ArrayList<Country> a = fromJsonArray(Test, Country[].class);
        Country c = fromJsonString(Test1, Country.class);
        Log.e("arrdddd", c.toString());


    }


}
