package ConnectionHandler;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.im.thegate.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by MOhamed Nabil
 * Email m.nabil.fci2015@gmail.com
 * Mobile num 01117576576 / 01010753727 on 1/31/2016.
 */

public class Connector {
    public static String GET = "GET";
    public static String POST = "POST";
    static Context context;
    static RequestQueue queue;
    static String APIURL;
    private static Connector connector;
    String Lang;

    private Connector() {
    }

    public static Connector getInstance(Context c, String APIURL) {
        if (connector == null) {
            connector = new Connector();
            queue = Volley.newRequestQueue(c);
            context = c;
            Connector.APIURL = APIURL;
            return connector;
        } else return connector;
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    public void SendRequest(String api, String Method, final Map<String, String> fparams, final ServiceCallback callback, final SweetAlertDialog dialog) {

        RequestQueue queue = Volley.newRequestQueue(context);
        int finalMethod = Request.Method.GET;
        if (Method.equals(GET)) {
            finalMethod = Request.Method.GET;

        } else if (Method.equals(POST)) {
            finalMethod = Request.Method.POST;
        }
        //api=APIURL+api;
        Log.e("test", api);
        StringRequest stringRequest = new StringRequest(finalMethod, api,
                new Response.Listener<String>() {
                    String resp;

                    @Override
                    public void onResponse(String response) {
                        // process your response here
//                         resp=response;
                        Log.e("response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (status.equals("success"))
                                response = jsonObject.getString("data");

                            else {
                                response = "";
                                callback.onFail(jsonObject.getString("message"));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.e("response", response);

                        callback.onSuccess(response);
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //perform operation here after getting error
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return fparams;
            }

        };

        queue.add(stringRequest);

    }

    public void LoadCountries(SweetAlertDialog dialog, ServiceCallback callback) {
        Map<String, String> params = new HashMap<>();
        params.put("lang", Preferences.lang);
        SendRequest(APIURL + "country", Connector.POST, params, callback, dialog);
    }

    public void LoadCities(SweetAlertDialog dialog, ServiceCallback callback) {
        Map<String, String> params = new HashMap<>();
        params.put("lang", Preferences.lang);
        params.put("countryId", Preferences.country.getId());
        SendRequest(APIURL + "city", Connector.POST, params, callback, dialog);

    }

    public void loadCategories(SweetAlertDialog dialog, ServiceCallback callback) {
        Map<String, String> params = new HashMap<>();
        params.put("lang", Preferences.lang);

        SendRequest(APIURL + "category", Connector.POST, params, callback, dialog);
    }

    public void loadNewsCategories(SweetAlertDialog dialog, ServiceCallback callback) {
        Map<String, String> params = new HashMap<>();
        params.put("lang", Preferences.lang);

        SendRequest(APIURL + "newscategory", Connector.POST, params, callback, dialog);
    }

    public void loadHomeCoupons(SweetAlertDialog dialog, ServiceCallback callback) {
        Map<String, String> params = new HashMap<>();
        params.put("lang", Preferences.lang);
        params.put("countryId", Preferences.country.getId());

        SendRequest(APIURL + "gethomecopouns", Connector.POST, params, callback, dialog);
        // SendRequest(APIURL+"/gethomecopouns?countryId="+ 0,Connector.GET,callback);
    }

    public void loadBookCoupons(int page, String CategoryId, String CityId, SweetAlertDialog dialog, ServiceCallback callback) {
        Map<String, String> Params = new HashMap<>(4);

        Log.e("book", "service");


        if (CategoryId != null && !CategoryId.equals(""))
            Params.put("categoryId", CategoryId);

        if (CityId != null && !CityId.equals("-1"))
            Params.put("cityId", CityId);
        Params.put("countryId", Preferences.country.getId());

        Params.put("page", page + "");
        String service = "getbookcopouns";

        Params.put("lang", Preferences.lang);


        Log.e("book", APIURL + service);

        SendRequest(APIURL + service, Connector.POST, Params, callback, dialog);

    }

    public void loadCoupons(int page, String CategoryId, String CityId, SweetAlertDialog dialog, ServiceCallback callback) {
        Map<String, String> Params = new HashMap<>(4);
        if (CategoryId != null && !CategoryId.equals(""))
            Params.put("categoryId", CategoryId);
        if (CityId != null && !CityId.equals("-1"))
            Params.put("cityId", CityId);
        Params.put("countryId", Preferences.country.getId());
        Params.put("page", page + "");
        String service = "coupon";
        Params.put("lang", Preferences.lang);


        SendRequest(APIURL + service, Connector.POST, Params, callback, dialog);

    }

    public void loadNews(int page, String CategoryId, SweetAlertDialog dialog, ServiceCallback callback) {
        Map<String, String> Params = new HashMap<>(4);


        if (CategoryId != null && !CategoryId.equals(""))
            Params.put("categoryId", CategoryId);

        Params.put("countryId", Preferences.country.getId());

        Params.put("page", page + "");
        String service = "news";


        Params.put("lang", Preferences.lang);

        SendRequest(APIURL + service, Connector.POST, Params, callback, dialog);

    }

    public void loadHomeNews(SweetAlertDialog dialog, ServiceCallback callback) {
        Map<String, String> params = new HashMap<>();
        params.put("lang", Preferences.lang);
        params.put("countryId", Preferences.country.getId());

        SendRequest(APIURL + "gethomenews", Connector.POST, params, callback, dialog);
    }

    public void GetFeaturesByStoreId(String storeId, SweetAlertDialog dialog, ServiceCallback callback) {
        Map<String, String> params = new HashMap<>();
        params.put("lang", Preferences.lang);
        params.put("storeId", storeId);

        SendRequest(APIURL + "getstorefeatures", Connector.POST, params, callback, dialog);

    }

    public void GetAvailableStoresbyCId(String Cid, SweetAlertDialog dialog, ServiceCallback callback) {
        Map<String, String> params = new HashMap<>();
        params.put("lang", Preferences.lang);
        params.put("cid", Cid);

        SendRequest(APIURL + "getstoresbycopounid", Connector.POST, params, callback, dialog);

    }

    public void getHomeBook(SweetAlertDialog dialog, ServiceCallback callback) {
        Map<String, String> params = new HashMap<>();
        params.put("lang", Preferences.lang);
        params.put("countryId", Preferences.country.getId());

        SendRequest(APIURL + "homebook", Connector.POST, params, callback, dialog);

    }


    public void loadStores(int page, String CategoryId, SweetAlertDialog dialog, ServiceCallback callback) {
        Map<String, String> Params = new HashMap<>(4);

        Params.put("countryId", Preferences.country.getId());

        if (CategoryId != null && !CategoryId.equals(""))
            Params.put("categoryId", CategoryId);

        if (Preferences.userlocation != null) {
            Params.put("lat", Preferences.userlocation.latitude + "");
            Params.put("lng", Preferences.userlocation.longitude + "");

        }

        Params.put("page", page + "");
        String service = "store";

        Params.put("lang", Preferences.lang);
        // SendRequest(APIURL+"/gethomecopouns?countryId="+ Preferences.country.getId(),Connector.GET,callback);
        SendRequest(APIURL + service, Connector.POST, Params, callback, dialog);
    }


    public void login(SweetAlertDialog dialog, String email, String pass, ServiceCallback callback) {
        Map<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("password", pass);

        SendRequest(APIURL + "login", Connector.POST, params, callback, dialog);

    }

    public void Register(SweetAlertDialog dialog, String email, String password,
                         String age, String name, String gender, String country_code, String phone, ServiceCallback callback) {
        Map<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("age", age);
        params.put("name", name);
        params.put("gender", gender);
        params.put("phone", phone);
        params.put("password", password);
        params.put("country_code", country_code);


        SendRequest(APIURL + "register", Connector.POST, params, callback, dialog);

    }

}

